# Apprentissage Énactif Anatomie Cardiaque

## Organisation du projet

- répertoire `doc` : documentation d'installation et de prise en main de la démonstration, ainsi que les documents de gestion de projet (cahier des charges, document de passation...)
- répertoire `src` : le code source de l'extension CamiTK
- répertoire `data` : images médicales et maillages utilisés pour créer les objets 3D nécessaires pour la visualisation et l'impression 3D

Il existe également un [google drive](https://drive.google.com/drive/folders/1xf80IwwkXI8fxsoZ0s84Awh4X7b5pTdj?usp=drive_link) <!--  (partagé avec compte gmail de Emmanuel.Promayon@u-ga.fr)  --> avec les documents de travail.
