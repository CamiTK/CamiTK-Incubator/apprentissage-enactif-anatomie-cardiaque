/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef APPRENTISSAGEENACTIF_H
#define APPRENTISSAGEENACTIF_H

#include <ActionExtension.h>

class ApprentissageEnactif : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.Apprentissage Enactif.action.ApprentissageEnactif")

public:
    /// Constructor
    ApprentissageEnactif() : ActionExtension() {};

    /// Destructor
    virtual ~ApprentissageEnactif() = default;

    /// Method returning the action extension name
    virtual QString getName() override {
        return "Apprentissage Enactif";
    };

    /// Method returning the action extension description
    virtual QString getDescription() override {
        return "More specifically, this project aims to understand and find the location and orientation of the heart in the thoracic cavity in a playful way.";
    };

    /// initialize all the actions
    virtual void init() override;

};

#endif // APPRENTISSAGEENACTIF_H


