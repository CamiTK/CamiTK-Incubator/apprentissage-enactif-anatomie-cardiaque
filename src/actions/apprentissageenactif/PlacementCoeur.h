/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef PLACEMENTCOEUR_H
#define PLACEMENTCOEUR_H

#include <Action.h>
#include <HardwareControllerComponent.h>

#include <QPushButton>
#include <QMutex>

class PlacementCoeur : public camitk::Action {
    Q_OBJECT

public:

    /// Default Constructor
    PlacementCoeur(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~PlacementCoeur() = default;

    /// this method creates and returns the widget containing the user interface for the action
    virtual QWidget* getWidget();

    /// manage change in the action parameters (modify the viewers)
    virtual bool event(QEvent*);

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of PlacementCoeur (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply() override;

    /// set the rotation angle to the given degree (between -180/+180)
    /// or the translation value to the given value [-1,1]
    void controllerAction(HardwareController::ControllerAction, double);

    /// a trigger action is activated (e.g., the start or stop actions)
    void controllerAction(HardwareController::ControllerAction, bool);

    /// generic change signal, received when a generic named state has changed.
    /// This comes directly from the controller and is custom/specific to each controller.
    /// PoseControl understands the "pose" states that consists in 7 double values:
    /// (x,y,z, qx, qy, qz, qw) (position and rotation expressed as a quaternion)
    /// Any other state change are ignored.
    /// @param data a QVariantMap where each item has a QString key (the state name) and a QVariant (the new value)
    void stateChanged(QVariantMap data);

    /// when game is stopped, show final result
    void showScore();

private:
    /// the current hardware controller component
    HardwareControllerComponent* controller;

    /// currently controlled component
    camitk::Component* currentComp;

    /// the property that holds all the opened component (as enum)
    camitk::Property* componentList;

    /// stop the game
    QPushButton* stopGameButton;

    /// the list of all the components stored in the same order as the component name enum
    QList<camitk::Component*> manageableComponents;

    /// target position/orientation (solution to reach)
    double xTarget, yTarget, zTarget;
    double qxTarget, qyTarget, qzTarget, qwTarget;

    // current position and orientation (required for continuous controllers)
    double x, y, z;
    double rx, ry, rz;

    /// distance between the target and current pose in terms of translation (mm)
    double deltaPosition;
    /// distance between the targe and current pose in terms of rotation (0..180°)
    double deltaOrientation;

    // to manage multiple call to stateChanged (sensor is in a different thread)
    QMutex updateMutex;

    /// update action widget display
    void update();

    /// to avoid multiple call to the event loop when property changes
    bool blockEvents;

    /// reset transform to identity
    void resetTransform();

    /// compute barycenter (expressed in world coordinates) of currently selected component
    /// @return true if center was computed (i.e. currentComp is a MeshComponent or ImageComponent)
    bool getBarycenter(double center[3]);

    /// Rotate of w degree around the (x,y,z) axis
    void rotateWXYZ(double angle, double x, double y, double z);

    // evaluation as a percentage score (0% = bad, 100% great)
    double percentDistance();
    double percentAngle();
    double percentScore();

    // return 0 to 5 stars depending on the given percentage
    QString getStars(double percentage);
};

#endif // PLACEMENTCOEUR_H

