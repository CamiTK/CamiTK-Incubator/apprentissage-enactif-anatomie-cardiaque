/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "PlacementCoeur.h"

// CamiTK includes
#include <Property.h>
#include <Application.h>
#include <ActionWidget.h>
#include <InteractiveGeometryViewer.h>
#include <ImageComponent.h>
#include <Log.h>

// Additional includes
// Qt
#include <QVector3D>
#include <QVector4D>
#include <QVBoxLayout>
#include <QMessageBox>

// VTK
#include <vtkCamera.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkQuaternion.h>

#include <string>

using namespace camitk;


// ------------------- Constructor -------------------
PlacementCoeur::PlacementCoeur(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Placement Coeur");
    setDescription("Replacer le coeur dans la cage thoracique");
    setComponentClassName("HardwareControllerComponent");

    // Setting classification family and tags
    setFamily("Anatomy");

    // Setting classification family and tags
    addTag("Coeur");
    addTag("Enactif");
    addTag("Game");
    addTag("Heart");
    addTag(tr("Medical Training"));

    // init
    controller = nullptr;
    currentComp = nullptr;
    blockEvents = false;

    // current position (after last update)
    x = y = z = rx = ry = rz = 0.0;

    // Coordonnées fixes de la cible (solution)
    xTarget = -270.;
    yTarget = -260.;
    zTarget = 75.0;
    qwTarget = -0.5;
    qxTarget = -0.5;
    qyTarget = 0.5;
    qzTarget = 0.5;

    // enum with custom list of all the current images
    componentList = new Property("Component", QVariant::StringList, "List of opened components", "");
    // Set the enum type
    componentList->setEnumTypeName("ComponentList");
    // Add the property as an action parameter
    addParameter(componentList);

    // device information
    Property* actionProperty = new Property("Action", QString(""), "Last action performed on the component", "");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    actionProperty = new Property("Status", "Disconnected", "Either Disconnected, Deactivated, or Activated", "");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    actionProperty = new Property(tr("Translation"), QVector3D(0.0, 0.0, 0.0), tr("Current translation (x,y,z)"), "");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    actionProperty = new Property(tr("Rotation"), QVector3D(0.0, 0.0, 0.0), tr("Around X-Y-Z rotation angle"), "degree");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    actionProperty = new Property(tr("Quaternion"), QString(""), tr("Rotation expressed as a quaternion (w, x, y, z)"), "degree");
    actionProperty->setReadOnly(true);

    addParameter(actionProperty);
    actionProperty = new Property("Δ Position", QString(""), "Distance au coeur", "mm");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    actionProperty = new Property("Δ Orientation", QString(""), "0 = orientation parfaite, 1 = 180° d'écart", "mm");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    //Ajout de la ligne deltaPosition à l'extension
    actionProperty = new Property("Score Position", QString(""), "Ce champ affiche votre score de positionnement", "mm");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    //Ajout de la ligne deltaOrientation à l'extension
    actionProperty = new Property("Score Orientation", "", "Ce champ affiche votre score d'orientation", "°");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    //Ajout de la ligne score à l'extension
    actionProperty = new Property("Score Global", "", "Ce champ affiche votre score global (tenant compte de la position et de l'orientation)", "");
    actionProperty->setReadOnly(true);
    addParameter(actionProperty);

    // be notified automatically when the parameters change
    setAutoUpdateProperties(true);
}

// ------------------- stateChanged -------------------
void PlacementCoeur::stateChanged(QVariantMap data) {
    if (updateMutex.tryLock() && data.contains("pose")) {
        blockSignals(true);
        actionWidget->blockSignals(true);

        // get the current pose
        QStringList poseList = data["pose"].toString().split(" ");
        float newPose[7];
        for (int i = 0; i < poseList.size(); i++) {
            newPose[i] = poseList[i].toFloat();
        }
        x = poseList[0].toFloat();
        y = poseList[1].toFloat();
        z = poseList[2].toFloat();
        double qw, qx, qy, qz;
        qw = poseList[3].toFloat();
        qx = poseList[4].toFloat();
        qy = poseList[5].toFloat();
        qz = poseList[6].toFloat();

        //-- Set new translation
        vtkSmartPointer<vtkMatrix4x4> newTransform = vtkSmartPointer<vtkMatrix4x4>::New();
        newTransform->SetElement(0, 3, x);
        newTransform->SetElement(1, 3, y);
        newTransform->SetElement(2, 3, z);

        //-- Compute Rotation matrix from quaternion
        vtkQuaternionf q(1.0f);
        // Beware pose expected format is  qw, qx, qy, qz as expected by VTK
        q.Set(qw, qx, qy, qz);
        float rotationMatrix[3][3];
        q.ToMatrix3x3(rotationMatrix);

        //-- Set new rotation
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                newTransform->SetElement(i, j, rotationMatrix[i][j]);
            }
        }

        //-- udpate component frame
        currentComp->getTransform()->SetMatrix(newTransform);

        //-- update orientation state
        double orientation[3]; // Euler angle
        currentComp->getTransform()->GetOrientation(orientation);
        rx = orientation[0];
        ry = orientation[1];
        rz = orientation[2];
        setProperty("Action", QString("Pose Acquired %1").arg(QDateTime::currentDateTime().time().toString("H:mm:ss")));

        //-- compute score
        // euclidean distance in mm (0 = exact, 200 = 20 cm...)
        deltaPosition = sqrt((xTarget - x) * (xTarget - x) + (yTarget - y) * (yTarget - y) + (zTarget - z) * (zTarget - z));
        // distance between two orientation / quaternions is not that easy to compute
        // see https://math.stackexchange.com/questions/90081/quaternion-distance
        // Here we are using theta (in radian) = geodesic distance of the normed quaternions, see also phi_6 in https://link.springer.com/article/10.1007/s10851-009-0161-2
        // $\theta \;=\; \cos^{-1}\bigl(2\langle q_1,q_2\rangle^2 -1\bigr) $
        //
        // range of theta i [0, PI] converted to degree
        deltaOrientation = acos(2.0 * pow(qw * qwTarget
                                          + qx * qxTarget
                                          + qy * qyTarget
                                          + qz * qzTarget, 2.0) - 1.0) * 180.0 / M_PI ;

        setProperty("Translation", QVector3D(x, y, z));
        setProperty("Rotation", QVector3D(rx, ry, rz));
        setProperty("Quaternion", QString("(%1 %2 %3 %4)").arg(QString::number(qw, 'g', 2)).arg(QString::number(qx, 'g', 2)).arg(QString::number(qy, 'g', 2)).arg(QString::number(qz, 'g', 2)));
        setProperty("Δ Position", QString("%1 mm").arg(QString::number((int) deltaPosition)));
        setProperty("Δ Orientation", QString("%1°").arg(QString::number((int) deltaOrientation)));
        setProperty("Score Position", getStars(percentDistance()));
        setProperty("Score Orientation", getStars(percentAngle()));
        setProperty("Score Global", getStars(percentScore()));

        update();

        dynamic_cast<ActionWidget*>(actionWidget)->update();
        Application::refresh();
        QCoreApplication::processEvents();
        actionWidget->blockSignals(false);
        blockSignals(false);

        // unlocking the mutex to reactivate update again
        updateMutex.unlock();
    }
}

//------------------- showScore -------------------
void PlacementCoeur::showScore() {
    QMessageBox msgBox;
    msgBox.setText(QString("<p>Votre score est :</p><ul><li>Δ Position : %1</li><li>Δ Orientation : %2</li><li>Score Position : %3</li><li>Score Orientation : %4</li><li>Score Global : %5</li></ul>").arg(property("Δ Position").toString())
                   .arg(property("Δ Orientation").toString())
                   .arg(property("Score Position").toString())
                   .arg(property("Score Orientation").toString())
                   .arg(property("Score Global").toString()));

    msgBox.exec();
}

// ------------------- percent -------------------
double PlacementCoeur::percentDistance() {
    double outbound = 550.0; // distance in mm that is considered outside the bound
    if (deltaPosition > outbound) {
        // over a meter away!
        return 0.0;
    }
    else {
        // 0mm → 100%, outbound mm → 0%
        return (1.0 - deltaPosition / outbound) * 100.0;
    }
}

double PlacementCoeur::percentAngle() {
    // 0° → 100%, 180° → 0%
    return (1.0 - deltaOrientation / 180.0) * 100.0;
}

double PlacementCoeur::percentScore() {
    double cumulative = percentDistance() + percentAngle();
    return (cumulative / 2.0);
}


// --------------- getStars -------------------
QString PlacementCoeur::getStars(double percentage) {
    // compute star systems
    if (percentage < 20.0) {
        return QString("☆☆☆☆☆");
    }
    else if (percentage < 50.0) {
        return QString("★☆☆☆☆");
    }
    else if (percentage < 65.0) {
        return QString("★★☆☆☆");
    }
    else if (percentage < 80.0) {
        return QString("★★★☆☆");
    }
    else if (percentage < 95.0) {
        return QString("★★★★☆");
    }
    else {
        return QString("🌟🌟🌟🌟🌟");
    }
}

// --------------- getWidget -------------------
QWidget* PlacementCoeur::getWidget() {

    // update the image list
    manageableComponents.clear();
    QStringList componentNameList;

    for (Component* comp : Application::getTopLevelComponents()) {
        if (comp->getRepresentation() == Component::GEOMETRY || comp->getRepresentation() == Component::SLICE) {
            componentNameList << comp->getName();
            manageableComponents.append(comp);
        }
    }

    componentList->setAttribute("enumNames", componentNameList);

    // stop game button
    stopGameButton = new QPushButton("Stop Game");
    connect(stopGameButton, &QPushButton::released, this, &PlacementCoeur::showScore);

    QWidget* defaultUI = Action::getWidget();
    QWidget* customUI = new QWidget();

    QVBoxLayout* vLayout = new QVBoxLayout(customUI);
    vLayout->addWidget(defaultUI);
    vLayout->addWidget(stopGameButton);

    return customUI;
}

// ------------------- apply -------------------
Action::ApplyStatus PlacementCoeur::apply() {
    if (controller == nullptr) {
        controller = qobject_cast<HardwareControllerComponent*>(getTargets().last());
    }

    if (controller->signalsConnected()) {        
        controller->disconnectSignals();

        blockEvents = true;
        setProperty("Action", "<b>Select a component and click Apply to start</b>");
        setProperty("Status", "Deactivated");
        blockEvents = false;

        // unset the previously selected one if it exists
        if (currentComp != nullptr) {
            resetTransform(); // unapply the transformation to the current component
            currentComp = nullptr;
        }
    }
    else {
        // connect this action slots to the controller signals
        controller->connectSignals(this);

        blockEvents = true;
        setProperty("Action", "");
        setProperty("Status", (controller->getStateValue(HardwareController::Start) != 0.0 ? "Activated" : "Deactivated"));
        blockEvents = false;
        
        currentComp = manageableComponents.value(property("Component").toInt());

        update();
    }
    return SUCCESS;
}

// ---------------------- event ----------------------------
bool PlacementCoeur::event(QEvent* e) {

    if (currentComp == nullptr || blockEvents) {
        return QObject::event(e);
    }

    // this is important to continue the process if the event is a different one
    return QObject::event(e);
}

// ------------------- controllerAction -------------------
void PlacementCoeur::controllerAction(HardwareController::ControllerAction action, double value) {
    double newValue;
    switch (action) {
        case HardwareController::XRotation:
            if (controller->isContinuous()) {
                // from [-180..+180] to [0..360]
                newValue = fmod((value + 360.0), 360.0);
            }
            else {
                newValue = value;
            }
            //rotateWXYZ(newValue - rx, 1.0, 0.0, 0.0);
            currentComp->getTransform()->RotateWXYZ(newValue - rx, 1.0, 0.0, 0.0);
            rx = newValue;
            break;

        case HardwareController::YRotation:
            if (controller->isContinuous()) {
                // from [-180..+180] to [0..360]
                newValue = fmod((value + 360.0), 360.0);
            }
            else {
                newValue = value;
            }
            // rotateWXYZ(newValue - ry, 0.0, 1.0, 0.0);
            currentComp->getTransform()->RotateWXYZ(newValue - ry, 0.0, 1.0, 0.0);
            ry = newValue;
            break;

        case HardwareController::ZRotation:
            if (controller->isContinuous()) {
                // from [-180..+180] to [0..360]
                newValue = fmod((value + 360.0), 360.0);
            }
            else {
                newValue = value;
            }
            // rotateWXYZ(newValue - rz, 0.0, 0.0, 1.0);
            currentComp->getTransform()->RotateWXYZ(newValue - rz, 0.0, 0.0, 1.0);
            rz = newValue;
            break;

        case HardwareController::XTranslation:
            if (controller->isContinuous()) {
                // normalizing [-1..1] with distance
                vtkCamera* camera = dynamic_cast<InteractiveGeometryViewer*>(Application::getViewer("3D Viewer"))->getRendererWidget()->getActiveCamera();
                newValue = value * camera->GetDistance();

                // TODO use visible prop bounds or boundingbox instead
                // vtkRenderer* renderer = dynamic_cast<InteractiveGeometryViewer*>(Application::getViewer("3D Viewer"))->getRendererWidget()->GetRenderWindow()->GetRenderers()->GetFirstRenderer();
                // renderer->ResetCameraClippingRange();
                // double* vtkRenderer::ComputeVisiblePropBounds 	( 		)
            }
            else {
                newValue = value;
            }
            currentComp->getTransform()->Translate(newValue - x, 0.0, 0.0);
            x = newValue;
            break;

        case HardwareController::YTranslation:
            if (controller->isContinuous()) {
                // normalizing [-1..1] with distance
                vtkCamera* camera = dynamic_cast<InteractiveGeometryViewer*>(Application::getViewer("3D Viewer"))->getRendererWidget()->getActiveCamera();
                newValue = value * camera->GetDistance();
            }
            else {
                newValue = value;
            }
            currentComp->getTransform()->Translate(0.0, newValue - y, 0.0);
            y = newValue;
            break;

        case HardwareController::ZTranslation:
            if (controller->isContinuous()) {
                // normalizing [-1..1] with distance
                vtkCamera* camera = dynamic_cast<InteractiveGeometryViewer*>(Application::getViewer("3D Viewer"))->getRendererWidget()->getActiveCamera();
                newValue = value * camera->GetDistance();
            }
            else {
                newValue = value;
            }
            currentComp->getTransform()->Translate(0.0, 0.0, newValue - z);
            z = newValue;
            break;

        default:
            break;
    }

    if (!controller->isContinuous()) {
        // TODO compute measured... using the transform matrix
        double measuredX, measuredY, measuredZ, measuredRx, measuredRy, measuredRz;
        // TODO compute/display the drift with x,y,z,rx,ry,rz
        // TODO set back the value directly in the transform to reduce the drift
    }
    setProperty("Rotation", QVector3D(rx, ry, rz));
    setProperty("Translation", QVector3D(x, y, z));
    setProperty("Action", QString("%1 = %2").arg(HardwareController::getEnumString(action)).arg(value));
    update();
    dynamic_cast<ActionWidget*>(actionWidget)->update();
}

void PlacementCoeur::controllerAction(HardwareController::ControllerAction action, bool value) {
    if (value) {
        setProperty("Action", HardwareController::getEnumString(action));
        setProperty("Status", (controller->getStateValue(HardwareController::Start) ? "Activated" : "Deactivated"));
        dynamic_cast<ActionWidget*>(actionWidget)->update();
        if (action == HardwareController::Reset) {
            resetTransform();
        }
    }
}

// ------------------- rotateWXYZ -------------------
void PlacementCoeur::rotateWXYZ(double angle, double x, double y, double z) {
    double axis_Frame[4] = { x, y, z, 1.0};
    double axis_World[4];
    currentComp->getTransformFromWorld()->GetMatrix()->MultiplyPoint(axis_Frame, axis_World);
    CAMITK_INFO(QString("axis_Frame: (%1 %2 %3) → axis_World: (%4 %5 %6). angle: %7").arg(x).arg(y).arg(z).arg(axis_World[0]).arg(axis_World[1]).arg(axis_World[2]).arg(angle))
    currentComp->getTransform()->RotateWXYZ(angle, axis_World[0], axis_World[1], axis_World[2]);
    currentComp->getTransform()->Modified();
}

// ------------------- update -------------------
void PlacementCoeur::update() {
    if (!Application::isAlive(currentComp)) {
        currentComp = nullptr;
    }

    // initialize current state values from the component
    if (currentComp != nullptr) {
        currentComp->refresh();

        vtkRenderer* renderer = dynamic_cast<InteractiveGeometryViewer*>(Application::getViewer("3D Viewer"))->getRendererWidget()->getRenderWindow()->GetRenderers()->GetFirstRenderer();
        renderer->ResetCameraClippingRange();
    }
}

// ---------------------- resetTransform ----------------------------
void PlacementCoeur::resetTransform() {
    if (currentComp != nullptr) {
        // reset component's value
        currentComp->resetTransform();
    }

    // reset HardwareControllerComponent values if this is a continuous device (not absolute)
    if (controller->isContinuous()) {
        x = y = z = rx = ry = rz = 0.0;
        controller->resetValue(HardwareController::XRotation);
        controller->resetValue(HardwareController::YRotation);
        controller->resetValue(HardwareController::ZRotation);
        controller->resetValue(HardwareController::XTranslation);

        // Reset the angle to zero
        blockEvents = true;
        setProperty("Translation", QVector3D(x, y, z));
        setProperty("Rotation", QVector3D(rx, ry, rz));
        blockEvents = false;
    }

    update();
}

// --------------- getBarycenter -------------------
bool PlacementCoeur::getBarycenter(double center[3]) {
    bool centerComputed = false;

    if (currentComp->getRepresentation() == Component::GEOMETRY) {
        //-- compute barycenter from point coordinates
        double sumCoord[3] = {0.0, 0.0, 0.0};

        for (vtkIdType i = 0; i < currentComp->getPointSet()->GetNumberOfPoints(); i++) {
            double* position = currentComp->getPointSet()->GetPoints()->GetPoint(i);

            for (unsigned int j = 0; j < 3; j++) {
                sumCoord[j] += position[j];
            }
        }

        for (unsigned int i = 0; i < 3; i++) {
            center[i] = sumCoord[i] / currentComp->getPointSet()->GetNumberOfPoints();
        }

        centerComputed = true;
    }
    else {
        ImageComponent* imageComp = dynamic_cast<ImageComponent*>(currentComp);
        if (imageComp) {
            // compute center using image bounds
            double bounds[6];
            imageComp->getImageData()->GetBounds(bounds);
            for (int i = 0; i < 3; i++) {
                center[i] = (bounds[i * 2 + 1] + bounds[i * 2]) / 2.0;
            }
            // add the transform to world
            vtkSmartPointer<vtkTransform> toWorld = imageComp->getTransformFromWorld();
            for (int i = 0; i < 3; i++) {
                center[i] += toWorld->GetMatrix()->GetElement(i, 3);
            }
            centerComputed = true;
        }
    }
    return centerComputed;
}
