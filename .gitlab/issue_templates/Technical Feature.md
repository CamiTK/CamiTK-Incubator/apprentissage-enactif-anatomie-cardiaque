| | |
|--|--|
| **As a** | [CamiTK developer/CEP developer/camitk-imp user...] |
| **I would like to** | [simple description of the technical feature] |
| **So that** | [simple description of what this feature brings to CamiTK] |
| **Epic/Topics** | [topic keywords or project epic] |


## Description / Overview

[enter your description or issue overview]


## Hints

[optional: enter a list of hints, urls or steps that can help solving this issue]


## Acceptance tests

[Please enter acceptance tests as TODOs. Acceptance test explains how to test that this issue is solved]
- [ ] [Test 1]
- [ ] [Test 2]
- [ ] [...]


## Track

[Please keep only one of the following label]

/label ~"Track None"

/label ~"Track Code Maintainability"
/label ~"Track Continuous Integration"
/label ~"Track Debugging"
/label ~"Track Dev Support"
/label ~"Track End User Experience"
/label ~"Track Knowledge Management"
/label ~"Track Prototyping Experience"
/label ~"Track Technology Integration"

## Misc

- Automatic subscription of issue creator:

/subscribe

**If appropriate, do not forget to mark this issue as "confidential"** by checking the corresponding tick box below
