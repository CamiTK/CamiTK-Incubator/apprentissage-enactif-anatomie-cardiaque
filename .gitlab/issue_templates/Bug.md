## About you
[Present yourself and your project you're working on with CamiTK]


## Overview
[Rewrite here a larger and more detailed restatement of your summary]


## Steps to Reproduce
[Write here the step - by - step process to reproduce the bug, including file to test (you can attach file on gitlab issue report system)]


## Actual VS Expected Result
[Write here the result of the step - by - step process and explain why it is not what you expected]


## Relevant logs and/or screenshots
[Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's very hard to read otherwise.]


## Interpretation & Possible fixes
[Write here your interpretation of this bug (If you can, link to the line of code that might be responsible for the problem)]


## CamiTK Version
[Copy/Paste the output of `camitk-config -b` CamiTK Version part ]








---
**please do not remove anything below this line**
/label ~Bug
/label ~"Track None"
