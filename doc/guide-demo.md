---
title: "Guide démo"
id: "camitk-whitepaper-style"
puppeteer:
    format: "A4"
    puppeteer: ["pdf", "png"]
export_on_save:
    puppeteer: false
    html: true
---

<!-- converted using https://word2md.com/ -->

# Guide de démonstration

#### Historique du document

|   | Author  | Date | Version
:--|:--|:--:|--|
Version initiale | PFE 2024 | 7 Jun 2024 | 1.0 |
Mise à jour pour JPO 2025 | Emmanuel Promayon | 14 Feb 2025 | 1.1 |

## Sommaire {ignore=true}

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=3 orderedList=false} -->

<!-- code_chunk_output -->

- [Matériel nécessaire](#matériel-nécessaire)
- [Montage](#montage)
- [Démarrage du logiciel](#démarrage-du-logiciel)
- [Étalonnage du capteur](#étalonnage-du-capteur)
- [Étalonnage de la position relative de la cage thoracique](#étalonnage-de-la-position-relative-de-la-cage-thoracique)
- [Déroulement du jeu](#déroulement-du-jeu)

<!-- /code_chunk_output -->

## Matériel nécessaire

Avant de commencer l’installation du TP, assurez-vous que vous avez l’ensemble des éléments suivants :

- Poste Linux sur lequel est installée l’extension du jeu, CamiTK et Visual Studio Code
- Les trois composantes magnétiques de localisation : émetteur, capteur et hub et deux câbles USB
- Les supports 3D : base de l'émetteur et du capteur, présentoir avec la cage thoracique (table + tablette en plastique + squelette) et modèle du cœur imprimé.

<!--page-break -->
<div style="display: block;break-after: always;page-break-after: always;"/>

## Montage

- Positionnez les éléments selon la figure 1 :
<div style="text-align: center;">
<img src="assets/plan-de-situation.png" width="70%"/>
<br/>
<strong>Figure 1.</strong> Montage du plan de travail
</div>

!!! warning Attention
    Cette figure correspond à l'ancienne démo, pour la démo courante, il faut poser la cage thoracique à plat sur la tablette en plastique, coccyx du côté de l'émetteur.

- Branchez le hub à l'ordinateur avec un câble USB-C
- Branchez l'émetteur à l'ordinateur ou au secteur avec un câble USB-C et assurez-vous que le voyant soit vert (ou qu’il clignote vert). Si le voyant clignote en blanc, c’est qu’il y a des perturbations magnétiques. Assurez-vous qu’il n’y a pas d’objets métalliques à proximité.
- Positionnez le capteur dans son emplacement dédié sur la base et l’allumer. Assurez-vous que le voyant soit vert.

## Démarrage du logiciel

- Lancez le poste Linux (le mot de passe pour accéder aux ordinateurs Linux est : _TIS@polytech_)

- ouvrez un terminal et tapez 

```bash
$DEMO_INSTALL_DIR/demo.sh
```

- Faire un clic droit sur `AmfiTrack Magnetic sensor` et sélectionner `Anatomy > Placement Coeur` (voir figure 2)

<div style="text-align: center;">
<img src="assets/demarrage-action.png" width="40%"/>
<br/>
<strong>Figure 2.</strong> Explorer de components, dans le panel gauche de CamiTK
</div>

## Étalonnage du capteur

- Allumer le capteur
- Pour vérifier le bon positionnement du montage, sélectionnez "capteur" comme `Component` et appuyer sur Apply.

<div style="text-align: center;">
<img src="assets/calibrage-capteur.png" width=30%/>
</div>

- Vérifier que le positionnement du capteur relativement à l'émetteur dans la scène 3D correspond à la réalité. Si ce n’est pas le cas, [il faut le recalibrer](https://guide.amfitrack.com/getting_started/getting_started.html#tracking-start) :
    - Allumer le capteur (appuyer sur le bouton pendant 1 seconde) si ce n'est pas déjà fait.
    - Positionner le capteur derrière l'émetteur (côté opposé à la LED) à une distance de 60 cm dans la direction des x positifs et à une hauteur de 30 cm (z positif), pointer le capteur en direction de la source, et appuyer sur le bouton à nouveau (les axes sont indiqués sous l'émetteur)
  
!!! Info
    Cet étalonnage peut être fait à tout moment (notamment si la connexion a été coupée) si vous vous rendez compte que les mouvements réels ne sont pas bien refléter dans la scène 3D.

## Étalonnage de la position relative de la cage thoracique

- Positionnez le capteur au niveau des points marqués en bleu sur la cage thoracique (haut et bas du sternum, vertèbres thoracique et extremum des côtes) et comparez avec leur positionnement dans la scène 3D de CamiTK (voir figure 4).

!!! Warning
    Dans la version actuelle de la démo, le squelette ne correspond pas au modèle 3D de CamiTK (il s'agit d'un mannequin d'apprentissage non spécifique)

- Positionnez le capteur sur l'extrémité du sternum de façon à ce qu'il pointe dans la direction du cou, et en étant bien centré sur ce point saillant
- Tout en maintenant le capteur sur ce point, repositionnez le squelette sur la tablette en plastique afin de bien le faire coïncider dans la scène 3D.

<div style="text-align: center;">
<img src="assets/calibrage-cage-thoracique.jpeg" width="55%"/>
<br>
<strong>Figure 3.</strong> Cage thoracique avec les points de repères
</div>

<!--page-break -->
<div style="display: block;break-after: always;page-break-after: always;"/>

## Déroulement du jeu

- Pour débuter le jeu, relancez la démo

<div style="text-align: center;">
<img src="assets/ecran-complet.png" width="75%"/>
<br>
<strong>Figure 4.</strong> Interface CamiTK du jeu "Placement Coeur"
</div>

!!! Info
    Dans l'interface de CamiTK :
    - `Ctrl+1` permet de n'afficher que la scène 3D
    - Clic gauche et souris dans la scène 3D pour faire tourner la caméra
    - Molette pour zoomer/dézoomer 
    - `Ctrl+0` permet de revenir à l'affichage complet avec les coupes axiales, coronales et sagittales.
    - Pour enlever les images scanner de la scène 3D : sélectionner la composante `scanner` dans l'explorateur à gauche, clic droit, menu `View`, `Viewer Visibility` et décocher `3D Viewer` (ou cliquer sur Ctrl+D)
    - Pour enlever le coeur positionné correctement de la scène 3D : la composante `coeur (solution)` dans l'explorateur, clic droit, menu  `View`, `Viewer Visibility` et décocher `3D Viewer`

- Pour lancer le jeu : faire un clic droit sur `AmfiTrack Magnetic sensor` et sélectionner `Anatomy > Placement Coeur` (voir figure 2), comme précédemment
- Cette fois-ci sélectionnez "coeur (mobile)" comme `Component` et appuyez sur "Apply". (voir figure 4)

<div style="text-align: center;">
<img src="assets/action.png" width="40%"/>
<br>
<strong>Figure 5.</strong> Interface CamiTK du jeu "Placement Coeur"
</div>

- Insérez le capteur dans l’emplacement dédié du cœur. Assurez-vous qu’il soit positionné de manière à avoir le voyant lumineux dans le fond du tunnel et dirigé vers le haut du coeur et le port usb-C vers l’extérieur.
- Une fois le capteur positionné, bloquez-le grâce à un morceau de carton ou un bout de papier plié. 
- Vous pouvez commencer à jouer.
- Lorsqu'on appuie sur `Stop Game`, une fenêtre s'affiche montrant le résultat (figure 6) courant au moment de l'appui sur le bouton. On peut refermer la fenêtre pour reprendre le cours du jeu

<div style="text-align: center;">
<img src="assets/score.png" width="30%"/>
<br>
<strong>Figure 6.</strong> Affichage du score
</div>