---
title: "Installation démo"
id: "camitk-whitepaper-style"
puppeteer:
    format: "A4"
    puppeteer: ["pdf", "png"]
export_on_save:
    puppeteer: false
    html: true
---

# Installation

#### Historique du document

|   | Author  | Date | Version
:--|:--|:--:|--|
Version initiale  | Emmanuel Promayon | 3 Dec 2024 | 1.0 |
Mise à jour pour JPO 2025 | Emmanuel Promayon | 14 Feb 2025 | 1.1 |

## Sommaire {ignore=true}

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=3 orderedList=false} -->

<!-- code_chunk_output -->

- [Étape 1 : Installation d'une machine Linux Ubuntu LTS 24.04 (ou Kubuntu 24.04)](#étape-1--installation-dune-machine-linux-ubuntu-lts-2404-ou-kubuntu-2404)
- [Étape 2 : Installation et compilation de CamiTK](#étape-2--installation-et-compilation-de-camitk)
  - [Installation des dépendances](#installation-des-dépendances)
  - [Clonage de Camitk develop](#clonage-de-camitk-develop)
  - [Build et installation](#build-et-installation)
- [Étape 3 : Installation de l'extension `hardware-controller`](#étape-3--installation-de-lextension-hardware-controller)
  - [Installation des dépendances](#installation-des-dépendances-1)
  - [Clonage de l'extension](#clonage-de-lextension)
  - [Build et installation](#build-et-installation-1)
- [Installation projet apprentissage énactif](#installation-projet-apprentissage-énactif)
- [Installation de l'environnement python](#installation-de-lenvironnement-python)
  - [Installation des dépendances dans un environnement virtuel python](#installation-des-dépendances-dans-un-environnement-virtuel-python)
  - [Gestion des droits utilisateur](#gestion-des-droits-utilisateur)
- [Lancement](#lancement)
- [Références](#références)

<!-- /code_chunk_output -->

## Étape 1 : Installation d'une machine Linux Ubuntu LTS 24.04 (ou Kubuntu 24.04)

## Étape 2 : Installation et compilation de CamiTK 

Une fois la mise en place de la machine Linux terminée, exécuter dans un terminal les commandes  suivantes :

### Installation des dépendances

```bash
sudo apt-get build-dep camitk
```

!!! Note 
    Si vous obtenez l'erreur "Impossible de trouver une source de paquet pour camitk", tapez la commande suivante pour installer les dépendances :
    ```bash
    sudo apt install lcov git lsb-release build-essential fakeroot gdb vim ninja-build clang iputils-ping traceroute cmake libvtk9-dev libvtk9-qt-dev qtbase5-dev libqt5xmlpatterns5-dev libqt5opengl5-dev qttools5-dev-tools xsdcxx libinsighttoolkit5-dev libfftw3-dev libgdcm-dev libgdcm-tools libvtkgdcm-dev xvfb xauth doxygen graphviz debhelper
    ```

### Clonage de Camitk develop

```bash
if [ -z "$DEMO_SRC_DIR" ]; then
cat <<EOF >> $HOME/.bashrc

# variables pour demo cœur
export DEMO_SRC_DIR=$HOME/demo/src
export DEMO_INSTALL_DIR=$HOME/demo/install
EOF
fi
source ~/.bashrc
mkdir -p $DEMO_SRC_DIR $DEMO_INSTALL_DIR
cd $DEMO_SRC_DIR
git clone -b develop --single-branch https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK.git 
```

### Build et installation

```bash
cd $DEMO_SRC_DIR/CamiTK
# configuration
cmake -S . -B build -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DCMAKE_INSTALL_PREFIX=$DEMO_INSTALL_DIR -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_JOB_POOL_COMPILE:STRING=compile -DCMAKE_JOB_POOL_LINK:STRING=link '-DCMAKE_JOB_POOLS:STRING=compile=5;link=2'
# build
cmake --build build --target camitk-ce-global-install 
```

!!! Note 
    Si la dernière étape (compilation) utilise trop de ressources et s'interrompt avant d'avoir terminée ou bloque complètement la machine, relancer la configuration avec `compile=3;link=1` pour limiter le nombre de processeurs utilisés

## Étape 3 : Installation de l'extension `hardware-controller`

### Installation des dépendances

```bash
sudo apt install libusb-1.0-0-dev pybind11-dev python3-pip python-is-python3
```

### Clonage de l'extension

```bash
cd $DEMO_SRC_DIR
git clone -b pybind11 --single-branch  https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK-Incubator/hardware-controller.git
```

!!! Tip
    Deux fenêtres de dialogue s'ouvrent pour entrer votre login et mot de passe sur gricad.
    ⚠ Dans la première fenêtre de dialogue il faut bien rentrer le login (même si le texte parle de mot de passe). Le mot de passe devra être rentré dans la 2e fenêtre de dialogue.

### Build et installation

```bash
cd $DEMO_SRC_DIR/hardware-controller
cmake -B build -S . -DCMAKE_INSTALL_PREFIX=$DEMO_INSTALL_DIR -G Ninja -DCMAKE_BUILD_TYPE=Release
cmake --build build --parallel --target camitk-hardwarecontrollercep-global-install
```

## Installation projet apprentissage énactif

```bash
cd $DEMO_SRC_DIR
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK-Incubator/apprentissage-enactif-anatomie-cardiaque.git
cd $DEMO_SRC_DIR/apprentissage-enactif-anatomie-cardiaque/src
cmake -B build -S . -DCMAKE_INSTALL_PREFIX=$DEMO_INSTALL_DIR -G Ninja -DCMAKE_BUILD_TYPE=Release
cmake --build build --parallel --target camitk-apprentissageenactif-global-install
```

!!! Warning Attention
    ⚠ Le projet est compilé et installé. Si vous faites des modifications dans le code source il faudra refaire l'étape d'installation (utilisation de la target de compilation `camitk-apprentissageenactif-global-install`)

## Installation de l'environnement python

### Installation des dépendances dans un environnement virtuel python

```bash
cd
python -m venv $DEMO_INSTALL_DIR/venv
source $DEMO_INSTALL_DIR/venv/bin/activate
pip install amfiprot amfiprot-amfitrack
pip list
deactivate
```

### Gestion des droits utilisateur

```bash
sudo usermod -a -G plugdev $USER
echo 'SUBSYSTEM=="usb", MODE="660", GROUP="plugdev"' | sudo tee /etc/udev/rules.d/50-pyusb.rules
sudo udevadm control --reload
sudo udevadm trigger
```

## Lancement

Créer un fichier `$HOME/Dev/install/demo.sh` et y copier le contenu suivant:

```bash
#!/bin/bash
if [ -z "$DEMO_INSTALL_DIR" ]; then
  echo "Attention: le répertoire d'installation est inconnu"
  exit
fi
source ${DEMO_INSTALL_DIR}/venv/bin/activate
killall QThread
${DEMO_INSTALL_DIR}/bin/camitk-imp ${DEMO_INSTALL_DIR}/share/camitk-*/testdata/demo-2025-02-15.camitk
killall QThread
deactivate
```

!!! Warning
    Attention de bien écrire ce fichier dans `$DEMO_INSTALL_DIR/demo.sh`

Puis le rendre exécutable :

```bash
chmod a+x $DEMO_INSTALL_DIR/demo.sh
```

Pour lancer la démo :
```bash
$DEMO_INSTALL_DIR/demo.sh
```

!!! Note
    Les données chargée par `demo.sh` (le workspace `demo-2025-02-15.camitk`) contiennent :
    - les modèles 3D de la base (support), de l'émetteur EMF, du capteur EMF, de la cage thoracique, du cœur positionné correctement dans la cage thoracique et du cœur à déplacer
    - l'image scanner de départ utilisée pour segmenter les organes

!!! tip
    Lire le document [guide-demo.md](guide-demo.md) pour plus d'information sur le déroulé de la démo.

!!! Note
    Si le capteur ne semble pas être au bon endroit par rapport à l'émetteur, [il faut le recalibrer](https://guide.amfitrack.com/getting_started/getting_started.html#tracking-start) :
    - Allumer le capteur (appuyer sur le bouton pendant 1 seconde) si ce n'est pas déjà fait.
    - Positionner le capteur "derrière" l'émetteur à une distance de 60 cm dans la direction des x positifs et à une hauteur de 30 cm (z positif), pointer le capteur en direction de la source, et appuyer sur le bouton à nouveau (les axes sont indiqués sous l'émetteur)

## Références

Quelques liens utiles si vous rencontrez des difficultés lors de l’installation :

- [installation de Linux dans Virtual Box](https://www.youtube.com/watch?v=v1JVqd8M3Yc)
- https://camitk.imag.fr/docs/install/linux-setup/ 
- https://camitk.imag.fr/docs/install/linux-install/#get-camitk-develop 
