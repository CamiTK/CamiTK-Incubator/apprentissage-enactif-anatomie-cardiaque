---
title: "CamiTK White Paper on ..."
id: "camitk-white-paper"
puppeteer:
    format: "A4"
    puppeteer: ["pdf", "png"]
export_on_save:
    puppeteer: true
---


# Projet PFE : Structuration d’un TP d’apprentissage énactif sur l’anatomie cardiaque


![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.001.png)![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.002.png)















<a name="_9171mm1qih4x"></a>**Cahier des charges**




# <a name="_sat9tzbht8kv"></a>Projet PFE : Structuration d’un TP d’apprentissage  énactif sur l’anatomie cardiaque 







Rédigé par ELHABASHY Rodaina, KASSA Seuad, LAURANT Mathilde, MORIN Emilie, VAGNEUR François, LETOURNEAU Léna, BAKKOU Ilias, IRDEL Julie




# <a name="_wo5jrt4sx6an"></a>Historique du document



<table><tr><th colspan="1" valign="top"><b>Version</b></th><th colspan="2" valign="top"><b>Rédigé par</b></th><th colspan="2" valign="top"><b>Vérifié par</b> </th><th colspan="2" valign="top"><b>Validé par</b> </th></tr>
<tr><td colspan="1" rowspan="2" valign="top"><p> </p><p>`     `1.0</p></td><td colspan="1" valign="top"><p>EL HABASHY Rodaina</p><p>KASSA Seuad, </p><p>LAURANT Mathilde,</p><p>MORIN Emilie, VAGNEURFrançois, </p><p>LETOURNEAU Léna, </p><p>BAKKOU Ilias, </p><p>IRDEL Julie</p><p></p></td><td colspan="1" valign="top">10/03/23</td><td colspan="1" valign="top"><p>MORIN Emilie</p><p></p><p>IRDEL Julie</p></td><td colspan="1" valign="top">20/03/23</td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><b>Motif et nature de la modification</b> : Création du document</td></tr>
<tr><td colspan="1" rowspan="2" valign="top"><p>    </p><p>`     `1.1</p></td><td colspan="1" valign="top">KASSA Seuad </td><td colspan="1" valign="top">27/04/23</td><td colspan="1" valign="top">PROMAYON Emmanuel</td><td colspan="1" valign="top">12/06/23</td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><b>Motif et nature de la modification :</b> III.5-Calcul du score</td></tr>
<tr><td colspan="1" rowspan="2" valign="top">`    `1.2</td><td colspan="1" valign="top">ELHABASHY Rodaina</td><td colspan="1" valign="top">3/05/23</td><td colspan="1" valign="top">PROMAYON Emmanuel</td><td colspan="1" valign="top">12/06/23</td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><b>Motif et nature de la modification :</b> Design du jeu</td></tr>
<tr><td colspan="1" rowspan="2" valign="top">`   `1.3</td><td colspan="1" valign="top">KASSA Seuad </td><td colspan="1" valign="top">12/06/23</td><td colspan="1" valign="top">LETOURNEAU Léna</td><td colspan="1" valign="top">16/06/23</td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>III-Le projet en pratique </p><p>`    `Vue d’ensemble</p><p>`    `Contrainte du système</p><p>`    `Versions du projet</p><p>`    `[Design du jeu	](#_4ybe51xd7ink)</p><p>[1. Propositions de maquettes	](#_eg96p5hlk08w)</p><p>[2. Les interactions	](#_6gjgwxidl2mh)</p><p>[3. Niveaux de difficulté](#_9gd05fgjkl3m)</p></td></tr>
<tr><td colspan="1" rowspan="2" valign="top">`   `1.4</td><td colspan="1" valign="top">Elhabashy Rodaina</td><td colspan="1" valign="top">15/06/23</td><td colspan="1" valign="top">LETOURNEAU Léna</td><td colspan="1" valign="top">16/06/23</td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>Ajout de la nouvelle Figure 1 : vue d’ensemble du projet</p></td></tr>
<tr><td colspan="1" rowspan="2" valign="top">`   `1.5</td><td colspan="1" valign="top">KASSA Seuad </td><td colspan="1" valign="top">15/06/23</td><td colspan="1" valign="top"><p>LETOURNEAU Léna</p><p></p><p>PROMAYON </p><p>EMMANUEL</p></td><td colspan="1" valign="top"><p>16/06/23</p><p></p><p>20/06/23</p></td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>Ajout de nouvelles parties dans le Calcul du score </p><p>[6. Calcul du score	](#_o5hobmqgjh3h)</p><p>` `[Formule pour le calcul de la distance par rapport au centre idéal du coeur dans la  cage thoracique	](#_4g65ac4cgiho)</p><p>[Formule pour le calcul des angles de rotation	](#_lxq5o0s40a93)</p><p>[Formule pour le calcul du score	](#_iald7lki5ajl)</p><p>`      `[Exemple d’application pour la formule du score](#_ohbprwpetnqd)</p></td></tr>
<tr><td colspan="1" rowspan="2" valign="top">1\.6</td><td colspan="1" valign="top">KASSA Seuad </td><td colspan="1" valign="top">20/06/23</td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>Ajout de la partie  Calcul du score en Annexe. </p><p></p></td></tr>
<tr><td colspan="1" rowspan="2" valign="top">`   `1.7</td><td colspan="1" valign="top">TANG Elisabeth </td><td colspan="1" valign="top">07/06/24</td><td colspan="1" valign="top"><p>RICEVUTO Léo</p><p></p></td><td colspan="1" valign="top"><p>07/06/24</p><p></p></td><td colspan="1" valign="top"></td><td colspan="1" valign="top"></td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>Modification de la partie  Calcul du score en Annexe et ajout de la tolérance pour les calculs de score. </p><p></p></td></tr>
</table>
#










# <a name="_kftf9i8fndey"></a><a name="_rz33rqr8kxvv"></a>


## Table of Contents {ignore=true}

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=3 orderedList=false} -->
<!-- code_chunk_output -->

- [     Objet du projet](#a-name_a9edo76rzynoa-----objet-du-projet)
- [](#a-name_6wyw3ssy29opa)
- [     Clients et autres parties prenantes](#a-name_gftuq3wspucca-----clients-et-autres-parties-prenantes)
- [     Les utilisateurs du produit](#a-name_n4i30epnd1yqa-----les-utilisateurs-du-produit)
- [     Vue d’ensemble](#a-name_etyevjdmbjmka-----vue-densemble)
- [     Scénarios envisagés](#a-name_aqcjzhx9oddpa-----scénarios-envisagés)
- [    Contraintes du système](#a-name_rqloh5xwvxara----contraintes-du-système)
- [     Versions du projet](#a-name_5rhgrrtdrbf4a-----versions-du-projet)
- [     Design du jeu](#a-name_4ybe51xd7inka-----design-du-jeu)
- [Calcul du score](#a-name_e0aa5q8qy2pcacalcul-du-score)
- [Capteurs](#a-name_ezqc4gmrpulracapteurs)
- [Visualisation](#a-name_zg9kqktjqw0gavisualisation)
- [Impression 3D](#a-name_jjrs0cqrqyu7aimpression-3d)
- [Interface utilisateur](#a-name_vpb8i189z3vjainterface-utilisateur)
- [](#a-name_3i5honbfze44a)
- [Utilisabilité](#a-name_5q8fzliehp32autilisabilité)
- [Fiabilité](#a-name_yfezzxq59vi8afiabilité)
- [Performance](#a-name_uuc4sfqwzu6waperformance)
- [Maintenabilité et support](#a-name_7eq4qhvwo7ihamaintenabilité-et-support)
- [](#a-name_1y2bfftmkhrwa)
- [Calcul du Score](#a-name_o0a03ofa7j5jacalcul-du-score)
  - [  Formule pour le calcul de la distance par rapport au centre idéal du coeur dans la  cage thoracique](#a-name_h9tv9wmae2dla--formule-pour-le-calcul-de-la-distance-par-rapport-au-centre-idéal-du-coeur-dans-la--cage-thoracique)
  - [       Formule pour le calcul des angles de rotation](#a-name_90xsrjfwi8w3a-------formule-pour-le-calcul-des-angles-de-rotation)
  - [     Formule pour le calcul du score](#a-name_iald7lki5ajla-----formule-pour-le-calcul-du-score)
- [Eléments technologiques liés aux éléments physiques et aux capteurs](#a-name_5f0f1zhn2f8zaeléments-technologiques-liés-aux-éléments-physiques-et-aux-capteurs)
  - [Détection d’un objet réel dans l'espace](#a-name_5f0f1zhn2f8zadétection-dun-objet-réel-dans-lespace)
- [Les capteurs de localisation 3D](#a-name_hbfzlarbo446ales-capteurs-de-localisation-3d)
- [L’impression 3D](#a-name_jqjrhqk4h1walimpression-3d)
- [L’imagerie et extraction des régions d'intérêts](#a-name_swjsa17jss1malimagerie-et-extraction-des-régions-dintérêts)
- [Visualisation imagerie](#a-name_z0gbs37u81d6avisualisation-imagerie)
- [Visualisation 3D](#a-name_o65d05a1yd2wavisualisation-3d)

<!-- /code_chunk_output -->

# <a name="_8pcy70h51bvx"></a>Table des figures

**Figure 1 :** Vue d’ensemble du projet

**Tableau 1 :** Représentation des différentes versions de notre projet

**Figure 2 :** Interface de CamiTK pour la V0 et la V1, mode de jeu facile, avant d’appuyer sur “Start”.

**Figure 3 :** Interface de CamiTK pour la V0 et la V1, mode de jeu facile, après avoir appuyé sur “Start”.

**Figure 4 :** Interface de CamiTK pour la V2, mode de jeu facile, avant d’appuyer sur “Start”.

**Figure 5 :** Interface de CamiTK pour la V2, mode de jeu facile, après avoir appuyé sur “Start”.

**Figure 6 :** Détection et  suivi d’un objet réel en mouvement à l’aide d’une webcam contrôlée par une carte Arduino

**Figure 7 :** Voiture autonome dotés de technologies de détection d’objets

**Figure 8 :** Principe du Lidar

**Figure 9 :** Fonctionnement des dispositifs de suivi de position optiques

**Figure 10 :** Recueil de données d’un capteur

**Figure 11 :** Réalisation d’un substitut osseux sur mesure par fabrication additive en PLA de grade médical à partir d’un fichier d’imagerie tridimensionnelle. 

**Figure 12 :** Modèle de cœur artificiel produit en poudre de polyuréthane thermoplastique (TPU). 

**Figure 13 :** Graphique de l’algorithme de segmentation de la cage thoracique. 

**Figure 14 :** Vue en projection et méthode d’estimation du couvrage de la cage thoracique.

**Figure 15 :** 3D Slicer

**Figure 16 :** ITK - SNAP

**Figure 17 :** OsiriX

**Figure 18 :** AMIRA Software

**Figure 19 :** Interface CamiTK

**Figure 20 :** Interface CamiTK
# <a name="_lohq6d3lrmj6"></a>I-Motivation du projet 
## <a name="_a9edo76rzyno"></a>     Objet du projet

`     `Ce projet a été attribué à des étudiants de la filière Technologies de l’Information pour la Santé (TIS) de Polytech Grenoble. Il a pour objectif de développer un système d'apprentissage énactif pour l'enseignement de l'anatomie humaine pour les étudiants de TIS. L'apprentissage énactif est une forme d'apprentissage qui se base sur les activités multi sensorielles motrices pour favoriser un apprentissage plus rapide et plus profond. Il permet aux apprenants d'acquérir des connaissances de manière active et immersive en utilisant leurs capacités motrices et sensorielles pour comprendre les concepts.

En d’autres termes, nous voulons créer un travail pratique (TP)  pour les étudiants de TIS durant lequel ils manipulent un cœur afin de mieux comprendre et appréhender la localisation et l’orientation du cœur dans la cage thoracique. Ce projet vise plus précisément à comprendre et à trouver la localisation et l'orientation du cœur dans la cage thoracique de manière ludique. L'objectif est d'offrir aux étudiants une meilleure vision et compréhension du cœur chez le patient sain qui leur permettra d'acquérir des connaissances anatomiques et physiologiques de manière plus active. Pour cela nous voulons que cet apprentissage se fasse sous la forme d’un jeu durant lequel les étudiants pourront manipuler le cœur. Ce projet permettra ainsi de surmonter les limites de l'enseignement traditionnel de l'anatomie basé sur des manuels ou des images 2D, en offrant une alternative plus ludique aux étudiants. Enfin, de manière plus globale, ce projet permettra de sensibiliser la population à cet organe essentiel pour notre santé et le bon fonctionnement de notre corps

`	`La mission principale de ce projet de fin d’étude est de spécifier les fonctionnalités, les éléments techniques, les composants matériels et les grandes étapes de réalisation. Cela consiste en la rédaction du cahier des charges.
## <a name="_6wyw3ssy29op"></a>	
## <a name="_gftuq3wspucc"></a>     Clients et autres parties prenantes

`	`Pour ce projet d’élaboration d’un TP énactif, notre premier client est ECCAMI, représenté par Emmanuel Promayon, enseignant-chercheur à l’Université Grenoble Alpes et responsable de la plateforme ECCAMI et Céline Fouard, enseignante-chercheuse membre de GMCAO (Gestes Médico-Chirurgicaux Assistés par Ordinateur) faisant partie de l’équipe d‘ECCAMI. 

`	`Fondé en 2010 par l'Université Grenoble Alpes, le CNRS et le CHU de Grenoble Alpes, ECCAMI (pour Excellence Center for Computer-Assisted Medical Interventions), regroupe les différents partenaires industriels régionaux et nationaux avec les chercheurs et cliniciens, du domaine des GMCAO.  

`    `L’une des missions d’ECCAMI est le conseil dans le prototypage logiciel et le transfert de technologies. Ce service est basé sur la plateforme logicielle open-source CamiTK (Computer Assisted Medical Intervention Tool Kit). [1]


`	`L’école d’ingénieur Polytech Grenoble est notre second client, puisque le projet sera à destination des étudiants en Technologies de l’Information pour la Santé(TIS). La filière TIS est représentée par Pascale Calabrese, enseignante-chercheuse à l’Université Grenoble - Alpes.

`	`L’École Polytechnique Universitaire de l'[Institut polytechnique de Grenoble](https://fr.wikipedia.org/wiki/Institut_polytechnique_de_Grenoble) ([UGA](https://fr.wikipedia.org/wiki/Universit%C3%A9_Grenoble-Alpes)) aussi appelée Polytech Grenoble est une école d’ingénieurs de Grenoble INP, institut d’ingénierie et de management de l'Université Grenoble Alpes. Polytech est aussi membre du réseau Polytech, qui regroupe 15 écoles d’ingénieurs universitaires en France. C’est une école publique habilitée par la Commission des Titres d’Ingénieur. [2]

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.001.png)![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.002.png)


## <a name="_n4i30epnd1yq"></a>     Les utilisateurs du produit

`	`Les futurs utilisateurs du produit seront les enseignants de l’école Polytech Grenoble ainsi que ses étudiants, lors de l’enseignement de l’anatomie humaine. Principalement, les professeurs et étudiants de la filière Technologies de l’Information pour la Santé de Polytech Grenoble. 

# <a name="_e0c488hpjx00"></a>
# <a name="_b2ds5osh93je"></a>II. Contraintes du projet

L’élaboration du projet doit répondre à certaines contraintes :  

- La mise en place d’une méthodologie Agile/Scrum a été demandée par le client dans la gestion du projet. 
- Un code en  C++ et une utilisation du logiciel CamiTk sont requis pour la réalisation du TP énactif. 
- Un budget de 3000€ est attribué au projet.
# <a name="_asso5fskxj08"></a>III. Le projet en pratique
## <a name="_etyevjdmbjmk"></a>     Vue d’ensemble

`   `Le but pédagogique de ce TP est de permettre aux étudiants de TIS de mieux comprendre et appréhender la localisation et l'orientation du cœur dans la cage thoracique. Cet apprentissage sera fait à travers une activité ludique où les étudiants pourront  manipuler un cœur imprimé en 3D et avoir un retour visuel à travers un logiciel de la localisation et de l’orientation du cœur 3D dans la cage thoracique 3D.  

`    `La figure 1 offre une vue d'ensemble du fonctionnement du produit final afin de faciliter la compréhension du travail pratique. Comme on peut le voir dans la figure ci-dessous, ce TP se compose de deux parties distinctes : une partie virtuelle et une partie physique. 

![](Images/Aspose.Words.592098ad-b7f0-42f5-bc3b-7dd171ed1442.003)

**Figure 1 :** Vue d’ensemble du projet



`   `La partie physique comprend un cœur imprimé en 3D, qui sera connecté à plusieurs capteurs pour obtenir en temps réel des informations sur sa position et son orientation. 

`   `D'autre part, la partie virtuelle se compose d'un logiciel qui permet la visualisation  de la position et de l’orientation en temps réel d'un cœur en 3D, en utilisant les données de localisation et de positionnement du cœur imprimé transmises par les capteurs. Cette visualisation du cœur en 3D pourra se faire soit dans une représentation en 3D d'une cage thoracique, soit à l'aide d'images médicales. Toutefois, le type de visualisation pourra être amené à évoluer, il s’agit ici de possibilité. Les interfaces pourront évoluer en ajoutant ou en enlevant une visualisation. De plus, le choix de la visualisation du cœur 3D dans le logiciel pourra dépendre du choix fait par l’enseignant ou par le système s’il détecte que l’élève n’arrive pas à positionner le cœur, le type de la  visualisation peut être changé par un autre.   Par ailleurs, il sera possible de choisir (par l’enseignant, l’élève ou le système lui-même) le niveau de difficulté (facile-intermédiaire-difficile) du jeu en fonction des avancées du projet. Voir la partie Design du jeu, la troisième sous-partie “[Niveau de difficulté](#_9gd05fgjkl3m)” pour plus de détails. 

## <a name="_aqcjzhx9oddp"></a>     Scénarios envisagés 

Lors du travail pratique, plusieurs scénarios de jeu sont possibles. A titre d’exemples, en voici quelques-uns. 

**Premier scénario envisagé** :  L’élève déplace dans l’espace un cœur imprimé relié à des capteurs et observe dans le logiciel son positionnement et son orientation dans la cage thoracique 3D. Grâce au logiciel, il a un feedback visuel et peut ajuster le positionnement et l’orientation en temps réel du cœur 3D en déplaçant le cœur imprimé 3D dans l’espace. Lorsqu'il estime avoir trouvé la bonne localisation et orientation, il peut valider la position du cœur directement sur l’interface du logiciel grâce à une interaction permettant la validation. Un score lui sera attribué en fonction de la précision du positionnement du cœur.

**Deuxième scénario possible** :  Avant de commencer à manipuler le cœur imprimé en 3D, l’élève doit appuyer sur une interaction(bouton) dans l’interface du logiciel, afin que le minuteur démarre et que le cœur 3D prenne en considération les données envoyées par les capteurs du cœur imprimé en 3D. L’élève déplace dans l’espace un cœur imprimé relié à des capteurs et observe dans le logiciel son positionnement et son orientation en superposant les contours du cœur 3D dans des images médicales. Grâce au logiciel, il a un feedback visuel et peut ajuster le positionnement et l’orientation en temps réel du cœur 3D en déplaçant le cœur imprimé 3D dans l’espace. Le système valide automatiquement la position et l’orientation du cœur lorsque le joueur a correctement placé le cœur. Un score lui sera attribué en fonction de la précision du positionnement du cœur et du temps pris par le joueur.

**Troisième scénario considéré** : Avant de commencer à manipuler le cœur imprimé en 3D, l’élève doit appuyer sur une interaction (bouton) dans l’interface du logiciel, afin que le compte à rebours préalablement fixé par le système  à 3 minutes (ce n’est qu’une proposition de durée, à voir après avoir implémenté le système et fait des tests)  se lance et que le cœur 3D prenne en considération les données envoyées par les capteurs du cœur imprimé en 3D. L’élève déplace dans l’espace un cœur imprimé relié à des capteurs et observe dans le logiciel son positionnement et son orientation en superposant les contours du cœur 3D dans des images médicales. Grâce au logiciel, il a un feedback visuel et peut ajuster le positionnement et l’orientation en temps réel du cœur 3D en déplaçant le cœur imprimé 3D dans l’espace. Au bout de 3 minutes, le système valide automatiquement la position et l’orientation du cœur. Cependant, si le compte à rebours n’est pas fini et que  le joueur estime avoir correctement positionné le cœur, il peut valider sa position directement sur l’interface du logiciel grâce à une interaction permettant la validation. Un score lui sera attribué en fonction de la précision du positionnement du cœur et du temps pris par le joueur.

**Quatrième scénario possible** :  Avant de commencer à manipuler le cœur imprimé en 3D, l’élève ou l'enseignant sélectionne sur l’interface graphique le niveau de difficulté du jeu (facile-intermédiaire-difficile). Ensuite, l’élève ou l'enseignant doit appuyer sur une interaction (bouton) dans l’interface du logiciel, afin que le compte à rebours préalablement fixé par le système  à 3 minutes se lance et que le cœur 3D prenne en considération les données envoyées par les capteurs du cœur imprimé en 3D. L’élève déplace dans l’espace un cœur imprimé relié à des capteurs et observe dans le logiciel son positionnement et son orientation en superposant les contours du cœur 3D dans des images médicales. Grâce au logiciel, il a un feedback visuel et peut ajuster le positionnement et l’orientation en temps réel du cœur 3D en déplaçant le cœur imprimé 3D dans l’espace. Au bout de 3 minutes, le système valide automatiquement la position et l’orientation du cœur. Cependant, si le compte à rebours n’est pas fini et que  le joueur estime avoir correctement positionné le cœur, il peut valider sa position directement sur l’interface du logiciel grâce à une interaction permettant la validation. Un score lui sera attribué en fonction de la précision du positionnement du cœur et du temps pris par le joueur.




## <a name="_rqloh5xwvxar"></a>    Contraintes du système

Au-delà des contraintes du projet, il est également important de prendre en compte les contraintes inhérentes au système à développer.

Les contraintes pour ce système comprennent :

- **Impression en 3D du cœur physique** : Il est nécessaire de disposer d'une imprimante 3D fonctionnelle et des ressources nécessaires pour fabriquer le cœur en 3D.

- **Connexion aux capteurs** : Le cœur imprimé en 3D doit être capable de se connecter à plusieurs capteurs pour obtenir en temps réel des informations sur sa position et son orientation. Il faut donc que les capteurs soient compatibles avec le cœur imprimé et qu'ils puissent communiquer efficacement avec lui.

- **Capteurs et les 6 degrés de liberté** : Le système doit être équipé de capteurs adéquats pour mesurer les mouvements et les rotations du cœur imprimé en 3D dans les six axes de liberté (trois translations et trois rotations). 

- **CamiTK** : Imposé par le client, le logiciel doit être développé en utilisant CamiTk comme composant clé de l'interface utilisateur. 
- **L'utilisation du langage C++** en tant que langage de programmation pour le développement du logiciel est également une contrainte imposée par le client.

- **Compatibilité matérielle et logicielle** : Le système doit être compatible avec le matériel et les logiciels utilisés (CamiTk). Cela peut inclure des exigences spécifiques en termes de système d'exploitation, de pilotes de périphériques, de langages de programmation ou de bibliothèques graphiques. La compatibilité doit être vérifiée pour assurer un fonctionnement harmonieux du système.


- **Transmission des données** : Les données de localisation et de positionnement obtenues à partir des capteurs doivent être transmises de manière fiable au logiciel de visualisation de la partie virtuelle. Il peut être nécessaire d'utiliser des protocoles de communication appropriés pour garantir une transmission des données sans erreurs.
- **Performances en temps réel :** Le système doit être capable de fournir des informations de position et d'orientation du cœur en temps réel. Cela signifie que le logiciel de visualisation doit être réactif et capable de traiter rapidement les données transmises par les capteurs pour afficher les changements en temps réel.

- **Extension de visualisation** : La partie virtuelle du système sera composée du logiciel CamiTK qui est imposé par le client et qui nécessite une extension  capable de visualiser en temps réel la position et l'orientation du cœur en 3D. Cette extension à développer doit être capable de recevoir les données transmises par les capteurs et de les afficher de manière appropriée. De plus, elle doit être flexible pour permettre l'ajout ou la suppression de différentes formes de visualisation, que ce soit une représentation en 3D d'une cage thoracique ou des images médicales.

- **Interaction utilisateur** : Le système doit offrir une interface conviviale et intuitive pour permettre aux utilisateurs de manipuler le cœur 3D et d'interagir avec le logiciel. Cela peut inclure des dispositifs d'entrée tels que des contrôleurs ou des boutons ainsi que des éléments visuels pour guider l'utilisateur dans la manipulation du cœur 3D.

- **Flexibilité de la visualisation** : Le logiciel de visualisation doit être capable de changer la méthode de visualisation du cœur 3D en fonction du choix de l'enseignant ou du système. Si l'élève a des difficultés à positionner le cœur, le logiciel peut automatiquement changer la visualisation pour une autre méthode. Cela implique que le logiciel soit suffisamment adaptable pour prendre en charge différentes formes de visualisation et qu'il puisse changer dynamiquement entre elles.

- **Évolutivité des interfaces** : Les interfaces utilisateur du logiciel de visualisation doivent être évolutives, ce qui signifie qu'il doit être possible d'ajouter ou de supprimer des visualisations sans compromettre la fonctionnalité globale du système. Les interfaces doivent être conçues de manière modulaire pour permettre une personnalisation facile.

Dans les  parties [**IV. Exigences fonctionnelles**](#_5w9pn7oh5wlc) et [**V. Exigences non fonctionnelles**](#_lg7b5yq1po3c), vous pourrez retrouver plus de détails quant aux exigences attendues pour mener à bien ce projet. 

## <a name="_5rhgrrtdrbf4"></a>     Versions du projet

`	`Nous avons imaginé incrémenter différentes versions reliées les unes aux autres afin de faciliter la réalisation de ce projet. Le tableau 1 ci-dessous présente en colonne les différentes versions du système de la V0 à la V4 et en ligne les différentes fonctionnalités du système. 
Cette vision à long terme proposée peut être remise en cause en fonction des avancées et des compétences des futurs étudiants reprenant le projet afin de l’implémenter. 

`   `La méthode Agile Scrum est importante dans ce contexte pour son adaptabilité. Comme mentionné, la vision à long terme du projet peut être remise en question en fonction des compétences et des avancées des futurs étudiants qui travailleront sur l'implémentation. La méthode agile permet d'ajuster et d'adapter les objectifs, les fonctionnalités et les priorités tout au long du processus de développement. Cela permet de répondre aux besoins changeants et d'incorporer les nouvelles idées et les leçons apprises au fur et à mesure de l'avancement du projet.

`	`La Version 0 (V0) sera la plus simple à réaliser, elle ne nécessitera que l’utilisation d’une interface (absence de capteur) dans le logiciel CamiTK. Ainsi, la cage thoracique et le cœur seront modélisés en 3D grâce aux outils déjà implémentés dans CamiTK. Plusieurs niveaux seront proposés à l’utilisateur afin d’augmenter la difficulté. Ces niveaux seront présents dans toutes les versions et un score sera calculé à la fin de chaque partie. Ce score prendra en compte le temps écoulé, la “finesse” des mouvements de l’utilisateur et l’exactitude des résultats. Pour garder un niveau de difficulté assez élevé, nous avons décidé qu’il y aurait les 6 degrés de liberté dès la V0 (voir Design du jeu pour en apprendre davantage sur cette partie). Pour moyenner les résultats du jeu, nous avions pensé à introduire 3 manches (3 rounds) au jeu, après quoi, le score de l’utilisateur sera calculé et s’affichera. 

**Résumé V0 :** 

- Absence de capteur
- Cage thoracique et coeur 3D visibles dans CamiTK
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 

`	`La V1 permettra par la suite d'introduire un capteur à la première version. Les utilisateurs pourront ainsi bouger directement le “cœur”, représenté par un seul capteur, dans l’espace. Cette étape introduira de nouvelles difficultés quant à l’exactitude de la retranscription du mouvement et de la position du cœur dans le logiciel. La V2, quant à elle, permettra d’ajouter des images médicales au logiciel (voir VI. Veille technologique pour comprendre le choix d’imagerie). Cela donnera l’occasion aux utilisateurs d’avoir accès à d’autres informations (notamment sur les tissus environnants) et d’apprendre à visualiser des images médicales. L’introduction de la fonctionnalité “Changement d’orientation de la cage thoracique sur le logiciel entre chaque essai”, sera étudiée par la suite selon la difficulté de son implémentation. 

**Résumé V1 :** 

- Un capteur
- Cage thoracique et coeur 3D visibles dans CamiTK
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 

**Résumé V2 :** 

- Un capteur
- Cage thoracique et coeur 3D + images médicales visibles dans CamiTK 
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 

`	`Dans la V3, nous augmenterons la difficulté en introduisant un cœur imprimé en 3D. Cela nécessitera davantage de capteurs. Après avoir recherché dans la littérature scientifique, nous avons décidé qu’il faudra utiliser du PLA (acide polylactique) et un dépôt de fil fondu ou une extrusion (FDM, FFF) (voir “Veille” - “Impression 3D”). Pour rendre le jeu plus réaliste, l’ajout d’une base de données a été étudié afin de stocker les meilleurs scores ainsi que les pseudos des étudiants. Ce dernier point reste optionnel, le plus important reste la bonne intégration des capteurs dans la version. 

**Résumé V3 :** 

- Plusieurs capteurs sur un coeur imprimé en 3D 
- Cage thoracique et coeur 3D + images médicales visibles dans CamiTK 
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 
- Base de données 

`	`La dernière version sera la plus complexe à réaliser. À la V3, nous ajouterons une cage thoracique physique, ce qui permettra une meilleure visualisation et un apprentissage plus concret. Il sera également envisageable d'intégrer un système de feedback par le cœur imprimé par l’ajout de lumières ou des vibrations. Cette version a été la moins travaillée, il sera donc important d’approfondir les recherches déjà effectuées.  

**Résumé V4 :** 

- Plusieurs capteurs sur un coeur imprimé en 3D 
- Cage thoracique et coeur 3D + images médicales visibles dans CamiTK 
- Cage thoracique physique pour placer le coeur 
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 
- Base de données 

Le tableau ci-dessous résume les différentes fonctionnalités de chacune de nos versions. 

|*Versions*|**V0**|**V1**|**V2**|**V3**|**V4**|
| -: | :-: | :-: | :-: | :-: | :-: |
|*Fonctionnalités*||||||
|Positionnement 3D du coeur dans le logiciel **CamiTK**|✅|✅|✅|✅|✅|
|Positionnement 3D du coeur dans le logiciel avec un **capteur** ||✅|✅|✅|✅|
|Positionnement 3D du coeur dans le logiciel avec un coeur imprimé en 3D (**plusieurs capteurs**)||||✅|✅|
|||||||
|Visualisation de la cage thoracique 3D et du coeur dans **CamiTK**|✅|✅|✅|✅|✅|
|Ajout d’**images médicales** |||✅|✅|✅|
|Utilisation d’une cage thoracique **physique**|||||✅|
|||||||
|**Feedback** dans l’écran de l’interface|✅|✅|✅|✅|✅|
|**Feedback** par le coeur 3D (vibration, lumière)|||||✅|
|||||||
|**6** degrés de liberté du cœur|✅|✅|✅|✅|✅|
|||||||
|Mise en place de **3 manches** pour le calcul du score d’une personne|✅|✅|✅|✅|✅|
|Possibilité de **bouger** la cage thoracique sur le logiciel|✅|✅|✅|✅|✅|
|Changement d’orientation de la cage thoracique sur le logiciel entre chaque essai (à voir à l’implémentation)|||✅|✅|✅|
|Ajout de pseudo et stock des meilleurs scores (optionnel)||||✅|✅|

**Tableau 1 :** Représentation des différentes versions de notre projet
## <a name="_4ybe51xd7ink"></a>     Design du jeu

`	`Dans cette partie, nous allons donner toutes les précisions se rapportant au design du jeu. Cela prend en compte le principe général du jeu mais également la partie Interface Homme-Machine, les différents niveaux de difficultés ainsi que le calcul du score. 

Le but de notre jeu est de trouver la bonne position et la bonne orientation du cœur en plaçant ce dernier dans une cage thoracique. L’utilisateur pourra sélectionner différents niveaux de difficultés pour apprendre et progresser dans le placement de cet organe complexe. Un score sera ensuite calculé selon différents critères permettant à l’utilisateur d’avoir un feedback direct sur ses progrès.

Tout notre programme sera dans le logiciel CamiTK, mais nous avons tout de même voulu donner une idée de ce à quoi notre interface pourrait ressembler dans la V0, V1 et V2. 

Pour rappel, dans la Version 0, seule la cage thoracique 3D sera visible (pas de capteurs et d’images médicales). Dans la V1, un capteur sera introduit, mais cela ne changera pas forcément l’interface de la V0 (sauf si des modifications sont nécessaires). La V2 sera cependant différente, puisqu'il y aura également les images médicales.  

Le détail des interfaces, des boutons et du fonctionnement du jeu se trouve ci-dessous.

1. ### <a name="_eg96p5hlk08w"></a>*Exemple d’interface* 

`	`Dans cette partie, nous avons imaginé à quoi pourrait ressembler notre interface pour la V0, la V1, ainsi que pour la V2. Ce design a été réalisé sur le logiciel Autodesk SketchBook. 

Les **Figures 2** et **3** représentent l’interface des versions 0 et 1 et les **Figures 4** et **5** représentent la version 2. 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.004.jpeg)![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.005.png)

**Figure 2 :** Interface de CamiTK pour la V0 et la V1, mode de jeu facile, avant d’appuyer sur “Start”.

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.006.png)![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.005.png)

**Figure 3 :** Interface de CamiTK pour la V0 et la V1, mode de jeu facile, après avoir appuyé sur “Start”.


![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.007.jpeg)

**Figure 4 :** Interface de CamiTK pour la V2, mode de jeu facile, avant d’appuyer sur “Start”.

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.008.jpeg)

**Figure 5 :** Interface de CamiTK pour la V2, mode de jeu facile, après avoir appuyé sur “Start”.

1. ### <a name="_6gjgwxidl2mh"></a>Les interactions

`	`Dans cette partie, nous allons détailler l’ensemble des informations concernant les éléments interactifs disponibles sur l’interface. 

- Élément interactif  ***“Coeur”** :* visible uniquement sur la V0 (en haut à gauche), il permettra de réinitialiser la position du cœur en haut à droite (voir **Figure 2**).

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.009.jpeg)

- Élément interactif “__Niveaux*”*__ *:* situé en bas à gauche, il  permettra de sélectionner le niveau de difficulté souhaité. Voir plus bas “Niveaux de difficulté” qui explique ces différents modes de jeu. 
- Élément interactif ***“Start”** :* comme son nom l’indique, il permettra de lancer le jeu et en même temps un chronomètre, non visible sur l’interface, mais qui permettra de calculer le score. 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.010.png)

- Élément interactif ***“Pause”** :* après avoir cliqué sur “Start”, il sera possible de mettre pause au jeu (et donc au chronomètre). Cela permettra notamment de faire tourner la cage thoracique pour mieux l’observer. Évidemment, le score final prendra en compte si l’utilisateur a utilisé cette option.

` `![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.011.png)

- Élément interactif ***“Stop”** :* Il permet d’arrêter le jeu. Peu importe à quel “round” on se situe, le jeu s’arrête totalement. L’interface de départ (voir **Figure 2** et **4**) s’affiche alors.

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.012.png)

- Élément interactif ***“Valider”** :* Il permet à l’utilisateur de valider le placement du cœur dans la cage thoracique. Trois rounds sont nécessaires pour terminer une partie. Le score, prenant en compte ces trois manches, sera ensuite calculé et affiché à l’utilisateur prenant en compte l’exactitude des placements, la “finesse” des mouvements  et le temps mis pour valider. 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.013.png)
1. ### <a name="_9gd05fgjkl3m"></a>Niveaux de difficulté 

`	`Nous avons imaginé mettre en place différents niveaux de difficulté qui seront disponibles dès la V0. 

`	`Pour le niveau le plus facile, il y aurait le cœur visible en transparence placé au bon endroit dans la cage thoracique (voir **Figure 2** à **5**). Celui-ci servirait de guide pour placer correctement le cœur. En plus de cela, il y aurait deux jauges de précision,une pour la translation et l’autre pour la rotation, (voir ci-dessous) qui permettront de renseigner à l’utilisateur si celui-ci se rapproche ou non du bon placement. 


**Résumé niveau facile :** 

- Délimitation du coeur en transparent 
- Présence aiguille précision pour aider au placement 
- Possibilité de bouger librement la cage thoracique dans toutes les rotations 


`   `Pour le niveau intermédiaire, le cœur serait plus grossièrement délimité (exemple : uniquement les contours visibles), rendant plus difficile son placement. L’aiguille de précision sera cependant toujours présente et la possibilité de bouger librement la cage thoracique dans toutes les rotations également.

**Résumé niveau intermédiaire :** 

- Délimitation grossière du coeur 
- Présence aiguille précision pour aider au placement 
- Possibilité de bouger librement la cage thoracique dans toutes les rotations 


`	`Pour le niveau difficile, il n’y aurait plus de délimitation du cœur, le joueur devra placer ce dernier comme il l’imagine dans la cage thoracique avant d’appuyer sur le bouton “Valider". L’aiguille de précision sera toujours présente mais moins précise (exemple : seulement 5 niveaux de précision présent) et la possibilité de bouger librement la cage thoracique dans toutes les rotations également.

**Résumé niveau difficile :** 

- Plus de délimitation du coeur 
- Présence aiguille précision pour aider au placement mais moins précise  
- Possibilité de bouger librement la cage thoracique dans toutes les rotations 


`	`Enfin, pour le niveau de difficulté expert, le joueur ne bénéficiera plus d’aucune aide pour placer le cœur. Il n’y aura donc plus de délimitation du cœur et ni d’aiguille de précision. En plus de cela, l’utilisateur ne pourra plus bouger la cage thoracique aussi librement qu’avant. 		Il aura accès à seulement 4 vues (frontale, dorsale, latérale droite et latérale gauche) par degré de rotation (x, y et z). Il y aura donc au total 12 vues différentes qu’il pourra changer avec les touches ou la souris (voir *Bouton “Appareil photo”* dans la rubrique “***Les boutons***” ci-dessus). 

**Résumé niveau expert :** 

- Plus de délimitation du coeur 
- Plus d’aiguille précision pour aider au placement
- Possibilité de bouger la cage thoracique dans 12 vues différentes

1. ### <a name="_paaudwzc07ze"></a>Placement du coeur 

`	`Le placement du cœur se fera via le capteur à partir de la V1, il n’est donc pas nécessaire de préciser comment le bouger puisque ce sera l’utilisateur qui l’orientera directement dans l’espace. Cependant, pour la V0, il est nécessaire de préciser comment placer le cœur via l’interface. 

Comme expliqué au dessus, il sera possible de bouger l’orientation de la cage thoracique, soit par le bouton “Appareil photo”, soit par la touche Ctrl (voir *Bouton “Appareil photo”* dans la rubrique “***Les boutons***” ci-dessus). 

De la même manière, nous avions pensé à bouger le cœur à l’aide de la touche Shift. Ainsi, pour des mouvements de rotation, la touche Shift doit être enfoncée et pour des mouvements de translation, la touche Shift doit être relâchée. 

Voici un exemple de comment bouger le coeur avec les touches : 

- Shift relaché + K ou M enfoncé -> bouge l’axe x
- Shift relaché + O ou L enfoncé -> bouge l’axe y
- Shift relaché + I ou P enfoncé -> bouge l’axe z
- Shift enfoncé + K ou M -> bouge l’axe de rotation x
- Shift enfoncé + O ou L -> bouge l’axe de rotation y
- Shift enfoncé + I ou P -> bouge l’axe de rotation z


Nous avions également pensé à prendre une manette de jeu (manette Xbox) pour bouger le cœur dans toutes les directions grâce aux joystick. Cela permettrait d’avoir un feedback direct par la manette (vibrations qui se rapprochent indique que l’utilisateur se rapproche du bon emplacement) et de faciliter les mouvements. Cependant, cela nécessiterait qu’il y ait autant de manettes que d’ordinateurs. Nous laissons donc les personnes qui reprendront ce projet, choisir si cela est une bonne option ou pas. 


1. ### <a name="_9pjzr6ymun5t"></a>Jauges de précision:
![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.014.png)

Nous avions décidé de mettre en place 2 jauges dans le jeu pour tous les modes.La première jauge sera pour la translation et la deuxième pour la rotation (Cf.Figure3)  le but des jauges c’est de guider le joueur et le prévenir si il est trop loin ou si il est proche de la bonne position En fonction de la couleur des jauges , le joueur verra apparaître sur l’ecran des message comme “ Fais attention tu es loin de la bonne position” ou le contraire .

Pour permettre de calculer le score à la fin ,on a associé chaque couleur de la jauge à des pourcentages de précision (cf.Figure6).

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.015.png)

**Figure 6 :** Tableau récapitulatif des pourcentages associés à chaque couleur de la jauge


1. ### <a name="_o5hobmqgjh3h"></a>Calcul du score
#### <a name="_m7xvo0dkk0k1"></a>         Etude anatomique du coeur 

Afin de proposer un score reflétant les performances du joueur à bien positionner le cœur dans la cage thoracique, il est nécessaire dans un premier temps, d’étudier l’anatomie du cœur ainsi que sa position et son orientation dans la cage thoracique. 

Le coeur est un localisé : 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.016.png)

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.017.png)

*Source : [Cardiovascular System: Location of the heart - YouTube*](https://www.youtube.com/watch?v=Y-rpzCpD-o8)*

- **Sternum** = Le coeur se trouve derrière le sternum, les ⅔ du coeur sont à droite et ⅓ à gauche du sternum.
- **2ème côte** = Limite supérieure du cœur au niveau de la 2ème côte, la base du cœur se trouve sous cette dernière. 
- **Vertèbres T5-T8** = Postérieurement le cœur repose sur le corps des vertèbres T5-T8.
- **L’apex du cœur** se trouve à gauche du sternum et repose sur le diaphragme 
- **6ème côte** = Limite inférieure du cœur au niveau de la 6ème côte. 


#### <a name="_4e2gydz8dfa3"></a>Degré de liberté et pénalité attribuée au score 

`	`Le joueur se verra attribuer un score final qui montrera ses performances lors du jeu: le bon positionnement du cœur dans la cage thoracique. 

Il faut donc  déterminer les pénalités attribuées par rapport aux 6 degrés de liberté (x,y,z). Par exemple, est ce que l’on va plus pénalisé le joueur s’il place le cœur plus en arrière; que s’il place le cœur trop à droite ? 

#### <a name="_4g65ac4cgiho"></a>  Formule pour le calcul de la distance par rapport au centre idéal du coeur dans la  cage thoracique

`    `Pour évaluer la position du cœur par rapport à la position idéale, nous utilisons la distance par rapport au centre idéal. Cette distance est calculée en comparant la position moyenne estimée du cœur avec sa position idéale dans la cavité thoracique, en utilisant des données de référence. Plus la distance est importante, plus le score sera pénalisé. Ainsi, en mesurant cette distance, nous pouvons déterminer si le cœur est correctement positionné ou s'il présente des écarts par rapport à la norme.  (voir [VIII- Annexe](#_990pdw5s2ffi) >  [Formule pour le calcul de la distance par rapport au centre idéal du coeur dans la cage thoracique](#_h9tv9wmae2dl) pour avoir la formule) .

#### <a name="_lxq5o0s40a93"></a>        Formule pour le calcul des angles de rotation 

`     `Pour estimer la rotation du cœur à placer par rapport à la bonne rotation du cœur, nous utilisons des angles de rotation de référence. Nous définissons des angles de rotation idéaux pour le cœur dans la cavité thoracique, puis nous calculons la différence entre ces angles et les angles idéaux pour la position du cœur. Plus l'écart entre les angles mesurés et les angles idéaux est important, plus le score sera pénalisé.

*Figure 7 : Les 6 degrés de liberté![](Images/Aspose.Words.592098ad-b7f0-42f5-bc3b-7dd171ed1442.019.png)*


- **Rotation autour de l'axe x** : L'axe x est généralement défini comme l'axe passant par l'apex du cœur et perpendiculaire **au plan frontal.** Un angle de rotation idéal pour cet axe serait de 0 degré, c'est-à-dire que l'apex du cœur serait dirigé vers le bas. Des angles de rotation positifs ou négatifs peuvent indiquer une position anormale du cœur dans le thorax.

- **Rotation autour de l'axe y** : L'axe y est généralement défini comme l'axe passant par l'apex du cœur et perpendiculaire **au plan sagittal**. Un angle de rotation idéal pour cet axe serait également de 0 degré, c'est-à-dire que l'apex du cœur serait dirigé vers l'avant. Des angles de rotation positifs ou négatifs peuvent indiquer une position anormale du cœur dans le thorax.

- **Rotation autour de l'axe z** : L'axe z est généralement défini comme l'axe passant par l'apex du cœur et perpendiculaire **au plan transversal.** Un angle de rotation idéal pour cet axe serait de 0 degré, c'est-à-dire que l'apex du cœur serait orienté vers la gauche. Des angles de rotation positifs ou négatifs peuvent indiquer une position anormale du cœur dans le thorax.

` `(voir [VIII- Annexe](#_990pdw5s2ffi) >  [Formule pour le calcul des angles de rotation](#_90xsrjfwi8w3)pour avoir la formule) .


# <a name="_5w9pn7oh5wlc"></a>IV. Exigences fonctionnelles
## <a name="_e0aa5q8qy2pc"></a>Calcul du score

`    `Le calcul du score est essentiel pour le feedback à l’utilisateur, il sera important d’approfondir cette partie car nous n’avons pas eu le temps d’aller au bout de nos recherches.

Pour calculer le score d’une partie, plusieurs éléments doivent être pris en compte. Le temps écoulé, l’exactitude des résultats et la finesse des mouvements lorsque le cœur est déplacé doivent être multipliés par des coefficients différents selon leur importance. 

Pour en apprendre plus sur cette partie, se référer à la partie **III. Projet en pratique - Design du jeu - Calcul du score** ci-dessus.
## <a name="_ezqc4gmrpulr"></a>Capteurs
`	`Afin que ce TP se réalise comme nous l’avons imaginé, il faut que les capteurs puissent fournir : 

- La position du coeur exacte au centimètre près et l’angle du coeur exact à 10° près
- A une fréquence de 10Hz (pour que l’utilisateur ne discerne pas ou très peu que l’affichage du mouvement est saccadé)

De plus, des exigences s’appliquent sur le capteurs en lui-même:

- le volume des récepteurs doit être inférieur à celui du coeur imprimé pour qu’ils puissent être placés dedans, ou peuvent être fixés sur le coeur s'ils ont un volume inférieur à 1cm3
- le poids des récepteurs ne doit pas dépasser 500g afin de rester un minimum manipulable
## <a name="_zg9kqktjqw0g"></a>Visualisation 
`    `Nous voulons pour notre projet pouvoir visualiser des modèles de cage thoracique et de cœur en 3D, des images médicales, la position de notre cœur dans l’espace grâce à la récupération des données des capteurs.

- Il faut que la visualisation des modèles 3D et des images se fassent sur la même page
- L’utilisation de CamiTK est la plus adaptée à la double visualisation.
- Il faut coder en C++ le jeu que nous voulons mettre en place étant donné que nous allons le rajouter à CamiTK qui est un logiciel Open-Source codé en C++
## <a name="_jjrs0cqrqyu7"></a>Impression 3D 

Les exigences concernant l’impression 3D sont présentes car nous voulons utiliser l’imprimante de Polytech pour réduire les frais. Vous trouverez ci dessous l’ensemble des exigences : 

- Le modèle 3D doit-être envoyé dans un fichier .gcode ou .stl
- La dimension de l’objet à imprimer ne doit pas dépasser 215 x 215 x 300 mm ce n’est donc pas possible d’imprimer une cage thoracique entière avec l’imprimante 3D de polytech. Il faudra donc faire partie par partie et les assembler ensemble.
- Il est fortement recommandé d’utiliser de l’Acide Polylactique pour l’impression.
# <a name="_lg7b5yq1po3c"></a>V. Exigences non fonctionnelles
# <a name="_n4q93b8465d8"></a>	
## <a name="_vpb8i189z3vj"></a>Interface utilisateur
- L’interface doit être intuitive. Les activités doivent pouvoir être lancées en moins de 3 clics après le lancement de l’application.
- L’ensemble des informations nécessaires à la réalisation du TP doivent être visibles sur la même fenêtre. Les options du jeu et les changements de difficultés sont accessibles via des boutons présents sur les menus principaux et font apparaître des pop-ups qui doivent être fermés avant de pouvoir continuer.
- Les couleurs utilisées doivent prendre en compte les personnes daltoniennes 
## <a name="_3i5honbfze44"></a>	
## <a name="_5q8fzliehp32"></a>Utilisabilité
- L’ensemble des différentes phases du TP doit pouvoir être réalisé par deux personnes, peu importe la  version du TP. 
- Les mouvements réalisés sur les modèles 3D doivent être répliqués sur les images médicales.
- Les manipulations des objets 3D ne doit pas requérir l’utilisation de plus de 3 touches en simultané 

## <a name="_yfezzxq59vi8"></a>Fiabilité 
- Un message d’erreur en cas de perte des capteurs doit apparaître et empêcher le TP de continuer. Dans l’idéal, il devrait être possible de perdre les capteurs et de s’y connecter sans devoir redémarrer le logiciel. 
- Les apprenants peuvent redémarrer l'exercice quand bon leur semble.
- Plusieurs groupes peuvent se suivre dans les manipulations sans avoir à redémarrer le logiciel 


## <a name="_uuc4sfqwzu6w"></a>Performance
- Le déplacement des modèles 3D doit être fluide pour permettre aux utilisateurs de suivre de façon confortable les différentes versions du TP.
- Le logiciel doit pouvoir fonctionner sur les ordinateurs de Polytech, principalement ceux de la salle 50 et 48 (salle de TP TIS).


## <a name="_7eq4qhvwo7ih"></a>Maintenabilité et support
- L’application doit fonctionner sous Windows dans un premier temps. Un déploiement sur d'autres systèmes d’exploitation (MacOS, Linux) est à envisager dans un second temps. 
- Le travail produit lors des premières version doit servir aux versions ultérieurs  


# <a name="_luealtm9f1ek"></a>
# <a name="_ngzp06ttjqis"></a>VII. Bibliographie

*Clients du projet*

[1]<http://eccami.imag.fr>

[2] <https://www.polytech-grenoble.fr/menu-principal/l-ecole/>


*Détection d’un objet réel dans l’espace*

[3]  [Détection et suivi d’un objet en mouvement à l’aide d'une webcam contrôlée par Arduino (robotique.tech)](https://www.robotique.tech/tutoriel/detection-et-suivi-dun-objet-en-mouvement-a-laide-dune-webcam-controlee-par-arduino/)

[4] [2022_defauw948.pdf (gretsi.fr)](http://gretsi.fr/data/colloque/pdf/2022_defauw948.pdf) 

[5] [⁣LiDAR, RaDAR ou capteurs à ultrasons, quelle technologie utiliser ? | SICK](https://www.sick.com/fr/fr/lidar-radar-ou-capteurs-a-ultrasons-quelle-technologie-utiliser-/w/comparatif-ultrasons-lidar-radar/)  

[6] [Comment choisir le meilleur capteur pour la détection des objets transparents (bannerengineering.com)](https://www.bannerengineering.com/fr/fr/company/expert-insights/selecting-clear-object-detection-sensors.html)  

[7] [Capteur de position - Fonctionnement et applications (mga-technologies.fr)](https://www.mga-technologies.fr/capteur-position/)  


*Capteurs de localisation 3D*

[8] <https://optitrack.com/applications/animation/>

[9] *<https://www.futurelearn.com/info/courses/music-moves/0/steps/12692>*

[10] <https://www.qualisys.com/cameras/miqus/>

[11] <https://optitrack.com/cameras/>

[12 et 13 dans] <https://www.sciencedirect.com/topics/computer-science/magnetic-tracking#:~:text=Electromagnetic%20tracking%2C%20also%20simply%20called,devoid%20of%20magnetic%20field%20distortions>.

[12] Bernhard Preim, Charl Botha, in Visual Computing for Medicine (Second Edition), 2014 : 18.4.4 Electro-Magnetic Tracking

[13] Rick Parent, in Computer Animation (Third Edition), 2012 : 6.1 Motion capture technologies

[14] <https://www.ndigital.com/electromagnetic-tracking-technology/>


*Impression 3D*

[15] “Use of 3D printing in oral and maxillofacial surgery”

R. Nicot, J. Ferri, G. Raoul sciencedirect.com/science/article/pii/S0001407921002740

[16] “Impression 3D en médecine régénératrice et ingénierie tissulaire”

Jean-Christophe Fricain, Hugo De Olivera, Raphaël Devillard, Jérome Kalisky, Murielle Remy, Virginie Kériquel, Damien Le Nihounen, Agathe Grémare, Vera Guduric, Alexis Plaud, Nicolas L’Heureux, Joëlle Amédée et Sylvain Catros

<https://www.medecinesciences.org/en/articles/medsci/full_html/2017/01/medsci20173301p52/medsci20173301p52.html>

[17] Kits de prothèse gratuits de doigts et de mains sur mesure

<http://enablingthefuture.org/>

[18] Les plastiques en impression 3D

<https://www.3dnatives.com/materiaux-impression-3d-abs-pla-polyamides-alumide/#>

[19] “Un coeur artificiel imprimé en 3D”

<https://fluidsandlubricants.com/2022/05/30/un-coeur-artificiel-imprime-en-3d/>

[20] “Les technologies d’impression 3D”

<https://www.aniwaa.fr/guide/imprimantes-3d/les-technologies-dimpression-3d/>

[21] “Imprimante 3D Ultimaker 3 extended”

<https://3dis-industrie.fr/produit/imprimante-3d-ultimaker-3-extended/>

[22] “Volume du coeur”

[https://fr.wikipedia.org/wiki/Cœur](https://fr.wikipedia.org/wiki/C%C5%93ur)


*Imagerie et extraction des régions d'intérêts*

[23] “Efficient Ribcage Segmentation from CT Scans Using Shape Features”

<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4486065/>

[24] Kang Y, Engelke K, Kalender WA. A new accurate and precise 3-D segmentation method for skeletal structures in volumetric CT data. Medical Imaging, IEEE Transactions on. 2003 May;22(5):586–598. [PubMed] [Google Scholar]

[25] Kiraly AP, Qing S, Shen H. A novel visualization method for the ribs within chest volume data. SPIE. 2006;6141 [Google Scholar]

[26] Staal J, van Ginneken B, Viergever MA. Automatic rib segmentation and labeling in computed tomography scans using a general framework for detection, recognition and segmentation of objects in volumetric data. Medical Image Analysis. 2007;11(1):35, 46. [PubMed] [Google Scholar]

[27] Lee J, Reeves AP. Segmentation of individual ribs from lowdose chest CT. SPIE. 2010;7624 [Google Scholar]

[28] Zhang L, Li X, Hu Q. Automatic rib segmentation in chest CT volume data. iCBEB 2012. 2012 May;:750–753. [Google Scholar]

[29] Chest CT-Scan images Dataset : CT-Scan images with different types of chest cancer

<https://www.kaggle.com/datasets/mohamedhanyyy/chest-ctscan-images>

[30] Abdominal Organ Location, Morphology, and Rib Coverage for the 5th, 50th, and 95th Percentile Males and Females in the Supine and Seated Posture using Multi-Modality Imaging

Ashley R. Hayes, F. Scott Gayzik, Daniel P. Moreno, R. Shayn Martin and Joel D. Stitzel

<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3861817/>

[31] “Définition de segmentation”

<https://www.synopsys.com/glossary/what-is-3d-image-segmentation.html>

[32] “OpenCV” [https://opencv.org](https://opencv.org/)/

[33] “ITK” <https://itk.org/>

[34][ “VTK” https://vtk.org/](https://vtk.org/).

[35] “SimpleITK” <https://simpleitk.org/>.

[36] “Projet de segmentation 3D en C++ sur GitHub”

https://github.com/search?q=image+segmentation+3D+in+c%2B%2B&type= 


*Visualisation imagerie*

[37] “3D Slicer”

<https://www.slicer.org>

[38] “ITKSnap”

<http://www.itksnap.org/pmwiki/pmwiki.php>

[39] “Osirix”

<https://www.osirix-viewer.com>

[40] “Amira Software”

<https://www.thermofisher.com/fr/fr/home/electron-microscopy/products/software-em-3d-vis/amira-software.html>

[41] “CamiTK”

<https://www.openhub.net/p/camitk>

[42] “CamiTK”

<https://rmll.ubicast.tv/videos/camitk/>


*Visualisation 3D*

[43] “Développement d’une application interactive en 3D pour l’apprentissage de l’ostéologie canine** - Université de Toulouse”

<https://oatao.univ-toulouse.fr/25406/1/Pearce_25406.pdf>

[44] “Méthode ISensor::GetData (sensorsapi.h)“

<https://learn.microsoft.com/fr-fr/windows/win32/api/sensorsapi/nf-sensorsapi-isensor-getdata>

[45] “Récupération des valeurs de données du capteur”

<https://learn.microsoft.com/fr-fr/windows/win32/sensorsapi/retrieving-sensor-data-fields>

[46] “Lecture et Écriture d’Images num ́eriques (Java, C++)”

<http://sparks.i3s.unice.fr/_media/public:gaignard:imagesio-2014.pdf>

[47] “Images in c++ with CImg library: Draw, display and event loop”

[Images in c++ with CImg library: Draw, display and event loop.](https://www.youtube.com/watch?v=Q9gqK4QEz3o)

[48] “Vidéo pour montrer comment coder l’interface”

[How to make the GUI: Text | C++ 3D Game Tutorial Series #40](https://www.youtube.com/watch?v=mrT8nnLpjZU)

## <a name="_1y2bfftmkhrw"></a>	

# <a name="_990pdw5s2ffi"></a>VIII- Annexe

## <a name="_o0a03ofa7j5j"></a>Calcul du Score
### <a name="_h9tv9wmae2dl"></a>  Formule pour le calcul de la distance par rapport au centre idéal du coeur dans la  cage thoracique



|<p></p><p>**DistanceDiff=** penaX\*(distance Xv - distance X )2 +penaY\* (distance Yv - distance Y)2 + penaZ\*(distance Zv- distance Z)2** </p><p></p>|
| :-: |

Où : 

Bonne position du coeur = (Xv, Yv, Zv )

Coeur à positionner  = (X, Y, Z) 

penaX, Y ou Z représentent les facteurs de pénalité correspondant à chaque dimension (X, Y et Z). Vous pouvez ajuster ces facteurs selon vos besoins pour attribuer différentes pondérations à chaque dimension dans le calcul de la distance.

Cette formule de score pour le calcul de la distance du cœur par rapport au centre idéal du coeur dans la cage thoracique, utilise la formule des distances entre les coordonnées (Xv, Yv, Zv ) du cœur  correctement positionné dans la cage thoracique avec les coordonnées (X, Y, Z ) du cœur à positionner par le joueur. Des pénalités selon les dimensions (X, Y et Z) ont été introduites. 


|Valeur de la distance|Interprétation|
| :-: | :-: |
|0-3|`                          `Idéale|
|`                          `4-6|` `Acceptable|
|`                          `6-9|`                `Modérément anormale|
|`                          `> 10|`  `Anormale|
**


`               `**Tableau 3 :** Interprétation des valeurs de la distance de la position du coeur

### <a name="_90xsrjfwi8w3"></a>       Formule pour le calcul des angles de rotation 

`    `Pour calculer l'écart global entre tous les angles idéaux et les angles mesurés pour la position du cœur, la formule suivante peut être utilisée :



|<p>**AngleDiff** =(penaX\*(x1 - x2)² +penaY\* (y1 - y2)² +penaZ\* (z1 - z2)²) </p><p></p>|
| :-: |


Dans cette formule, **x1, y1, z1** représentent les **angles idéaux** pour la position du cœur, et **x2, y2, z2** représentent les **angles mesurés** pour la position du cœur. Les opérations de soustraction et d'exponentiation au carré sont utilisées pour obtenir les différences entre chaque angle, puis ces différences sont sommées et la racine carrée est calculée pour obtenir l'écart global. De plus, penaX, Y ou Z représentent les facteurs de pénalité correspondant à chaque dimension (X, Y et Z). Vous pouvez ajuster ces facteurs selon vos besoins pour attribuer différentes pondérations à chaque dimension dans le calcul de l'écart global entre tous les angles idéaux et les angles mesurés pour la position du cœur.


L'unité de l'AngleDiff dépend de l'unité utilisée pour les coordonnées x, y et z dans le calcul. Dans la formule donnée précédemment, les coordonnées x, y et z représentent des positions dans un système de coordonnées tridimensionnel.

Si les coordonnées x, y et z sont exprimées en centimètres (cm), alors l'AngleDiff sera exprimé en centimètres carrés (cm²), car chaque terme dans la formule (x1 - x2)², (y1 - y2)² et (z1 - z2)² représente une différence de position au carré.




|Valeur de l'AngleDiff	|Interprétation|
| :-: | :-: |
|`       `0° - 5°|`                          `Idéale|
|`                           `5° - 10°|` `Acceptable|
|`                          `10-30°|`                `Modérément anormale|
|`                          `> 30°|`  `Anormale|



**Tableau 4 :** Interprétation des valeurs de la formule AngleDiff pour les angles  du coeur


### <a name="_iald7lki5ajl"></a>     Formule pour le calcul du score 

Pour intégrer tous les paramètres mentionnés (distance et angle de rotation) dans une formule globale pour l'évaluation de la position du cœur et donc du score, la formule suivante peut être utilisée qui est une combinaison de la distance au centre idéal et des écarts d'angles de rotation : 



|**Score** = w1 \* DistanceDiff + w2 \* AngleDiff|
| :-: |


Dans cette formule, les poids w1 et w2 peuvent être utilisés pour ajuster l'importance relative de la distance par rapport aux écarts d'angles de rotation.

La distance est la valeur obtenue à l'aide de la formule de distance au centre idéal mentionnée précédemment, où on  calcule la distance entre la position moyenne estimée et la position idéale du cœur dans le thorax.

AngleDiff représente les écarts entre les angles idéaux et les angles mesurés pour chaque axe de rotation mentionné, comme précédemment expliqué.  

En ajustant les poids et en déterminant des seuils appropriés pour la pénalité, l’attribution de scores plus élevés sera fait aux cas où la distance au centre idéal est importante ou lorsque les angles de rotation s'écartent considérablement des valeurs idéales, indiquant une position anormale du cœur dans le thorax.



|Valeur du Score|Interprétation|
| :-: | :-: |
|<p>`                  `Score < 2 </p><p></p>|Le cœur est très proche de la position idéale.|
|`                  `2 ≤ Score < 5 |Le cœur présente quelques écarts mineurs par rapport à la position idéale.|
|`                 `5 ≤ Score < 10|Le cœur présente des écarts modérés par rapport à la position idéale.|
|`                `Score ≥ 10 : |Le cœur est significativement éloigné de la position idéale.|


`                                           `**Tableau 5** : Interprétation des valeurs du score 

#### <a name="_ohbprwpetnqd"></a>Exemple d’application pour la formule du score 

Dans cet exemple d'application, nous utiliserons la distance au centre idéal et l'AngleDiff (la différence angulaire) pour calculer un score global.

Supposons que nous ayons les valeurs suivantes :

\- Distance : 10 cm

\- AngleDiff : 3.5 cm² 

Nous définirons les poids suivants :

\- w1 = 0.7 (pour la distance) (sans unité)

\- w2 = 0.3 (pour l'AngleDiff) (sans unité) 

Les poids w1 et w2 sont sans unité, car ils sont utilisés pour ajuster l'importance relative des termes dans le calcul du score et n'ont pas d'unité spécifique.

La formule pour calculer le score sera alors :

Score = 0.7 \* Distance + 0.3 \* AngleDiff

En substituant les valeurs, nous obtenons :

Score = 0.7 \* 10 + 0.3 \* 3.5 = 7 + 1.05 = **8.05** 

**Interprétation :** 


|`                 `5 ≤ Score < 10|Le cœur présente des écarts modérés par rapport à la position idéale.|
| :- | :-: |


Dans cet exemple, la distance contribue davantage au score en raison du poids plus élevé (0.7), tandis que l'AngleDiff a une contribution relativement plus faible en raison du poids plus bas (0.3).

Il est important de noter que les valeurs des poids et des mesures utilisées dépendent du contexte spécifique et doivent être ajustées en fonction des critères et des besoins<a name="_nssvpt5zksz8"></a>.

#### <a name="_tolerance"></a>Tolérance pour les calculs de score
 

|*Idéal *|**Acceptable **|**Modérement anormale**|**Anormale**|
| -: | :-: | :-: | :-: |
|*Calcul du score sur un seul axe en translation*||||
|de 0 à 4 cm|de 4 cm à 6.5 cm|de 6.5 cm à 13.5 cm|au dela de 13.5 cm|
|*Calcul du score sur un seul axe en rotation*||||
|de 0 à 18° |de 18° à 39°|de 39° à 93°|au dela de 93°|








## <a name="_5f0f1zhn2f8z"></a>Eléments technologiques liés aux éléments physiques et aux capteurs 
### <a name="_5f0f1zhn2f8z"></a>Détection d’un objet réel dans l'espace

La détection du cœur imprimé en 3D dans l’espace sera un élément important dans l’élaboration du travail pratique énactif. Une veille sur la détection d’un objet réel dans l’espace a donc été menée. 

Il existe de nombreuses technologies/techniques permettant la détection d’objets réels en mouvement dans l’espace, plus ou moins complexes et onéreuses. Parmi elles, nous pouvons trouver la détection et le suivi d’un objet réel en mouvement à l’aide d’une webcam contrôlée par une carte Arduino. Comme l’illustre l’image ci-dessous, un objet réel est détecté par la webcam, ce dernier va envoyer un flux d’images en temps réel vers l’ordinateur. L’ordinateur va procéder au traitement de ces images puis va envoyer l’ordre à la carte Arduino de faire tourner le servomoteur pour permettre à la webcam de suivre l’objet détecté.  

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.018.png)

**Figure 6 :** Détection et  suivi d’un objet réel en mouvement à l’aide d’une webcam contrôlée par une carte Arduino [3]

Dans le domaine automobile, les voitures autonomes sont dotées de technologies permettant la détection d’objets (voitures, camions, etc…) dans le but d’automatiser la conduite. En effet, elles sont munies de différents types de **capteurs de distance** (“range sensors”) tels que les lidars (“laser imaging detection and ranging”), des radars, des sonars ou des caméras stéréoscopiques. Ces capteurs permettent de positionner les obstacles dans l’espace grâce au calcul de la distance et de la position angulaire par rapport au capteur.  


![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.019.png)

**Figure 7 :**Voiture autonome dotés de technologies de détection d’objets [4]


Le Lidar pour Light (ou Laser Imaging) Detection And Ranging, est un instrument de télédétection active, qui trouve sa place dans de nombreux domaines (topographie, guidage automatique de véhicules, agronomie, ou encore dans l’étude de la pollution atmosphérique) permettant la détection d’objets pouvant être situés jusqu’à 200 m. Cette détection est permise par l’analyse des propriétés d’un laser (infrarouge) renvoyé par la cible vers son émetteur. Il est ainsi capable d’évaluer une zone en 2D ou 3D. 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.020.png)

**Figure 8 :** Principe du Lidar [5]

Dans le domaine industriel, la détection d’objets est utilisée dans de nombreux aspects tels que le tri, la gestion des stocks et de la qualité, processus d’emballage, etc. Pour ce faire, les **capteurs rétro-réflectifs** (capteurs photoélectriques à LED) sont utilisés dans la détection d’objets transparents. Le principe est simple : le capteur est constitué d’un émetteur et un récepteur. L’émetteur envoie un faisceau lumineux vers l’objet, qui le renvoie au récepteur. 

De plus, les **capteurs à ultrasons** sont également utilisés dans la détection d’objets, dans le domaine industriel. Ils mesurent les distances en écoutant l'écho de retour d'une onde sonore émise et réfléchie par l’objet. Ces derniers ont de nombreux avantages comme le fait qu’ils ne sont pas sensibles à la lumière ambiante et fonctionnent dans des environnements humides. D’autre part, ils sont insensibles à la couleur et à la transparence de l’objet. [6,7]

**Le capteur de position** est un dispositif recueillant des informations sur la position et le mouvement d’un objet. Ces informations sont obtenues par contact direct avec l’objet ou à distance de l’objet par l’usage de capteurs magnétiques. Le capteur de position permet d’apprécier l’épaisseur, l’angle de rotation d’un objet,...Le principe de fonctionnement reste simple : Le capteur de position émet un signal via un champ lorsqu’il n’est pas au contact de l’objet d’intérêt. Le champ peut être de différents types : électromagnétique, électrostatique ou bien un champ d’induction magnétique. Par ailleurs, il existe une multitude de capteurs de position, on peut retrouver: 

- Les **capteurs à ultrasons** cités plus haut,
- **Le capteur optique** qui se base sur la réception et l’émission d’un faisceau lumineux.
- Les **capteurs capacitifs** qui détectent des objets de nature quelconque.
- Les **capteurs pneumatiques** détectant des objets à faible distance. 
## <a name="_hbfzlarbo446"></a>Les capteurs de localisation 3D
1. ### <a name="_cuveb0isfnm2"></a>**Capteurs Optiques :** 

Il existe trois grandes catégories de systèmes de suivi optiques : les systèmes vidéo, infrarouge et laser. Les systèmes vidéos utilisent des caméras calibrées pour identifier et suivre des marqueurs placés sur les objets à suivre. Ces marqueurs sont de simples marques visuelles. Les systèmes infrarouges ont deux sous catégories selon le type de marqueur utilisé : les systèmes actifs, où les marqueurs émettent les ondes infrarouges et les systèmes passifs, où ces ondes partent de la caméra et sont réfléchis par les marqueurs.

Il existe une multitude  de systèmes de suivi optique connus, par exemple les systèmes infrarouges, utilisés en motion capture ou les caméras Time-of-Flight utilisées sur les dispositifs Kinect de Microsoft. 

Ces systèmes permettent une précision sub-millimétrique à une condition : une ligne de vue dégagée entre les caméras et l’objet à suivre (ou les capteurs placés sur cet objet). 

Ces dispositifs sont largement utilisés pour de la motion capture en animation 3D pour le jeu vidéo ou le cinéma [8]. Ces systèmes sont également utilisés dans le suivi des sportifs et dans l’analyse du mouvement humain. 

Le fonctionnement de ces systèmes est assez simple : une onde est envoyée par un émetteur qui peut aussi bien être une caméra qu’un marqueur. Cette onde est soit réfléchie par un marqueur ou la surface de l’objet et arrive à la caméra soit elle arrive directement au caméra. Le temps entre l’envoi d’une onde et sa réception permet d’avoir sa distance avec la caméra. En combinant plusieurs caméras et les informations données par celles-ci, on a la position de    l’objet en 3 dimensions. ![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.021.png)

**Figure 9 :** Fonctionnement des dispositifs de suivi de position optiques [9]

Il faut donc que chaque marqueur soit vu par au moins trois caméras à tout moment pour avoir sa position dans l’espace. En cas de petites pertes de données, il est possible de combler ces manquements en faisant une interpolation entre les points avant et après ce “trou” dans les données.

Le besoin de ligne de vue directe pose un problème non négligeable dans le cadre de notre solution. L’objet manipulable sera pris en main et, à terme, au sein d’une cage thoracique. Cela rend les lignes de vue très difficiles à maintenir à moins d’avoir un nombre de caméras bien trop élevé. Si les caméras sont à l’extérieur de la cage, les côtes bloqueraient la vue. Au sein de la cage thoracique, les caméras seraient trop volumineuses et empêcheraient le mouvement au sein de la cage thoracique. En effet, un set-up de motion tracking par infrarouge nécessite entre 3 et 18 caméras en général, bien qu’il n’y ait pas de maximum de caméras[10]. Les caméras disponibles sur le marché sont aussi relativement volumineuses (14.0 × 8.7 × 8.4 cm pour la Miqus, ou 6.86 x 6.86 x 2.3 pour le système OptiTrack par exemple). En l'état, il nous semble difficile d'utiliser ces dispositifs, d’autant plus que leur prix est prohibitif pour ce projet (6200€ par caméra pour le système Miqus, 1899 pour celles de Optitrack [11]. 

Pour finir, ces systèmes optiques sont particulièrement intéressants pour de nombreuses applications, notamment médicales, mais pas pour cette application. De plus, ces systèmes sont très populaires et de nombreuses ressources sont disponibles en ligne.

1. ### <a name="_xt8isbda91n7"></a>**Capteurs Électromagnétiques** 
Une des options permettant la localisation 3D d’un objet est l’utilisation de capteurs électromagnétiques. Ceux-ci mesurent la force du champ magnétique au point de l’espace où ils se situent [12]. 

`	`Ils sont constitués de plusieurs parties :

- un émetteur : composant qui produit produit 3 champs magnétiques orthogonaux
- un ou plusieurs récepteurs : chacun est constitué de 3 bobines orthogonales, dont la tension est alors due aux champs magnétiques de l’émetteur

Généralement, l’émetteur a une position fixe et les récepteurs sont fixés sur la cible dont on veut la localisation 3D.

Contrairement à d’autres types de capteurs, la mesure n’est pas affectée si il y a des obstacles dans la ligne de vue entre émetteur et récepteur. Toutefois, il est possible que le signal soit perturbé par la présence d’un objet conducteur dans le champ (comme les métaux). De plus, les capteurs électromagnétiques sont très difficiles à impossibles d’utilisation si un autre champ magnétique ou un objet ferromagnétique sont présents dans le champ de l’émetteur. La qualité de la détection des capteurs est également limitée par la gamme et la précision du champ magnétique de l’émetteur [13].

Pour effectuer la mesure, les récepteurs doivent communiquer leurs mesures à l’unité d’interface des capteurs. 2 méthodes existent :

- avec un ou plusieurs câbles, reliant les récepteurs à l’unité d’interface
- sans fil, ce qui nécessite que les récepteurs soient accompagnés d’une source d’énergie, souvent une batterie

Les 2 méthodes peuvent entraver le mouvement, surtout avec l’utilisation de câbles. C’est pourquoi il serait plus logique d’adopter le fonctionnement sans fil. 

`	`Voici un schéma des étapes de recueil des données par un capteur électromagnétique [14]:

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.022.png)

**Figure 10 :** Recueil de données d’un capteur




M.Promayon nous a recommandé l’utilisation du capteur électromagnétique Flock of Birds (FoB) par Ascension Technology Corporation. C’est un capteur EM de tracking très utilisé en réalité virtuelle, simulation et robotique. Il a l’avantage de correspondre à nos attentes en termes de précision et de résolution.

Leur production a été stoppée, leur produit de remplacement est le 3D Guidance trakSTAR / driveBAY. L’ensemble dont nous disposons semble fonctionner, il nous manque pour l’instant un câble RS232 afin de le connecter à un ordinateur et voir si les logiciels sont encore installables et s' ils sont compatibles avec la version des capteurs que nous possédons. 
## <a name="_jqjrhqk4h1w"></a>L’impression 3D

Selon l’article suivant, “Use of 3D printing in oral and maxillofacial surgery”, “L’impression 3D, également appelée fabrication additive, est une technique de fabrication de pièces en volume par rajout ou agglomération de matière. Elle permet l’obtention d’un objet physique à partir de données numériques obtenues par imagerie volumique” [15]. 

Pour des exigences esthétiques et fonctionnelles, c'est dans la chirurgie osseuse, et particulièrement orale et maxillofaciale, que l’impression 3D s’est tout d’abord développée. La réalisation de substituts osseux sur mesure a permis de rapides progrès dans la médecine et la chirurgie. Ces derniers permettent une meilleure adaptation dans le corps humain que les substituts ou greffes sculptés par le chirurgien [16] (voir **Figure 11**). 

Il est possible de trouver gratuitement sur Internet des kits sur mesure dans le but d’imprimer les différentes pièces d’une prothèse [17]. Ces fichiers numériques permettent de réduire considérablement le prix des prothèses par rapport à celles qu’on peut trouver dans le commerce.

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.023.jpeg)

**Figure 11 :** Réalisation d’un substitut osseux sur mesure par fabrication additive en PLA de grade médical à partir d’un fichier d’imagerie tridimensionnelle. 

L’adaptation du substitut créé par fabrication additive est parfaite (flèche) alors que l’autogreffe (étoile) est située à distance de l’os receveur ce qui peut compromettre sa stabilité et sa bio-intégration. PLA de grade médical : matériau plastique utilisable en clinique.

- L’acide polylactique ou PLA, est selon nous, le choix de matériel le plus judicieux pour ce projet. Ce plastique, facile d’utilisation, est le plus abordable de l’impression 3D et ainsi le plus utilisé. Il est fabriqué à partir de matières renouvelables (amidon de maïs) le rendant ainsi compostable en condition industrielle mais également recyclable. Si ce plastique est aussi populaire, c’est également pour ces faibles températures d’impression qui sont comprises entre 190°C et 230°C. De plus, son faible rétrécissement à l’impression 3D lui permet de ne pas utiliser de plateaux chauffants [18]. Même si certains articles ont souligné que d’autres matériaux, tel que le polyuréthane thermoplastique (TPU) [19] (voir **Figure 12**), peuvent donner un effet plus réaliste à l’organe imité, le but de notre projet est avant tout de pouvoir positionner correctement le coeur dans la cage thoracique et non de le reproduire à l’exactitude. 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.024.png)

**Figure 12 :** Modèle de cœur artificiel produit en poudre de polyuréthane thermoplastique (TPU). 

Cœur d’un patient réalisé par la start-up française 3DHeart Modeling sur une imprimante EOS.


Plus globalement, dans le monde médical, l’impression 3D est dorénavant largement utilisée permettant l’optimisation de la matière première, la fabrication d’objets complexes, la personnalisation des impressions, la réalisation de prototype etc.

Les imprimantes 3D sont également de plus en plus présentes dans les blocs opératoires. Elles sont très utiles pour la préparation d’interventions plus ou moins complexes que ce soit pour les chirurgiens peu expérimentés ou les plus entraînés. L’impression 3D permet également de modifier l’indication de traitement grâce à une réalité qui est augmentée par rapport aux examens d’imagerie classique.



Nous allons ici nous concentrer plus particulièrement à l’utilisation de l’impression anatomique 3D dans un environnement pédagogique afin de mieux comprendre l’état de l’Art dans le contexte de notre projet. 

Toujours selon l’article cité un peu plus haut, plus de 80 articles concernant l’impression 3D dans le domaine de la pédagogie médicale, ont été publiés au cours des 5 dernières années. Cela a particulièrement été le cas dans la chirurgie cardiovasculaire, craniofaciale et digestive ainsi que dans la traumatologie et l’orthopédie. Ces nombreux articles ont prouvé l’efficacité de l’impression 3D dans la compréhension et la reconnaissance des structures anatomiques dans l’espace par rapport aux méthodes traditionnelles. “Il a également été démontré que la mémoire des objets réels était nettement meilleure que leurs représentations bidimensionnelles [...] les modèles imprimés en 3D donnaient des informations spécifiques, liées à la perception tactile du support imprimé en 3D, soutenant son rôle dans le domaine de la pédagogie”.  

La start-up française 3DHeart Modeling a réalisé une véritable prouesse en termes de réalisme en reproduisant le cœur d’un patient, à partir d’une imprimante EOS (voir **Figure 12**). Leur objectif est de fabriquer des pièces à la fois fines, souples et poreuses dans le contexte pédagogique de la chirurgie. C’est pourquoi ils ont utilisé du polyuréthane thermoplastique TPU et la technologie SLS (selective laser sintering). Cette technique, que l’on peut également appeler frittage sélectif par laser, est basée sur l’utilisation d’un laser pour fritter un matériau sous forme de poudre, elle est ainsi capable de reproduire des parois très fines. Dans le contexte de notre projet, nous utiliserons du PLA, il serait ainsi préférable d’utiliser un dépôt de fil fondu ou une extrusion (FDM, FFF) pour reproduire notre coeur. Le principe de cette technique étant qu' “un filament plastique est fondu et déposé sur une plateforme d’impression, formant l’objet couche après couche” [20]. 

`	`En conclusion, l’impression 3D est devenue essentielle en médecine tout autant dans son usage pédagogique qu’en pratique clinique dans le but de mieux comprendre et d’aborder l’anatomie normale et pathologique.

Pour aller plus loin, nous pourrions nous étendre sur le bio-printing, large sujet de recherche de l’impression 3D qui a pour objectif de reconstituer des tissus et organes humains à partir de cellules et de biomatériaux naturels ou synthétiques.

Il est possible de faire l'impression 3D dans la salle de PolyBot au deuxième étage du bâtiment de Polytech Grenoble. L’impression est possible et même recommandé d’être faite en Acide Polylactique ce qui est parfait étant donné que c’est le matériel qui nous paraissait le mieux. Pour l’impression l’imprimante utilisée est la **Ultimaker 3 extended** qui est utilisée de tête d’impression (nozzle) d’un diamètre de 0,4mm.

L’imprimante **Ultimaker 3 extended** utilise la méthode de la double extrusion. Comparé à l’extrusion, cette méthode va permettre d’obtenir “des impressions complexes en associant deux types ou deux couleurs de filaments.”[21]. De plus, d'après la documentation technique, nous nous sommes rendu compte qu’il est possible de faire imprimer une pièce avec volume de 215 x 215 x 300 (197 x 215 x 300 mm en double extrusion). Sachant qu’un cœur fait environ 1,5 fois la taille d’un poing soit environ 105 x 98 x 205 mm il sera possible de l’imprimer en 3D [22]. L’Ultimaker 3 Extended est équipée d’une connexion Wifi afin de simplifier le transfert des fichiers vers l’imprimante. Les fichiers doivent être envoyés à Polytech en .gcode ou .stl.

Le point positif est que l’impression 3D est gratuite à Polytech si c’est pour un projet scolaire. Dans le cas où nous utilisons trop de plastique, une compensation financière peut être demandée.

## <a name="_swjsa17jss1m"></a>L’imagerie et extraction des régions d'intérêts

La collecte de données médicales est essentielle à notre projet. Nous devons fournir des images 3D compréhensibles et en adéquation avec notre modèle afin de le faire correspondre avec notre interface. Pour cela nous utiliserons des images de scanner anonymisées fournies par le CHU de Grenoble. 

Si nous reprenons le projet de 3DHeart Modeling (voir plus haut **Figure 12**), afin de reproduire à l’identique le cœur d’un patient, la start-up a réalisé une imagerie en coupe et notamment un scanner cardiaque, « offrant une visualisation spatiale en haute résolution ». Ils expliquent également qu’ils ont dû utiliser d’autres techniques telles que la résonance magnétique nucléaire et l’échographie tridimensionnelle cardiaque.

Notre objectif est de pouvoir visualiser la cage thoracique et de savoir avec exactitude la position et l’orientation du cœur. Même si notre projet est différent de celui de 3DHeart Modeling, l’utilisation d’un CT-scan ou d’un IRM serait les imageries que nous souhaiterions réaliser. 

Comme l’article “Efficient Ribcage Segmentation from CT Scans Using Shape Features” l’indique, la structure et la morphologie des os de la cage thoracique sont essentielles à la compréhension des nombreux systèmes associés, et notamment à la localisation des organes internes [23]. L’article décrit les algorithmes de segmentation de la cage thoracique que l’on peut trouver dans la littérature et qui permettent d’identifier les différentes structures osseuses et vasculaires [24] [25] [26] [27] [28]. 

Cependant, les auteurs soulignent un biais dans ces algorithmes, le manque de prise en compte des variations liées à la résolution, l’apparence et l’état pathologique du patient, créant ainsi des faux positifs et faux négatifs. L’article propose alors un autre algorithme permettant de pallier ces manques : “All bone structures are first segmented, then extra bones such as shoulder blade and sternum are detected and discarded from the previous segmentation, resulting in rib cage alone”. Leur but est donc de supprimer certains os de la cage thoracique qui pourrait créer du “bruit” et ainsi des erreurs dans la visualisation finale. Le résultat est calculé à l’aide de l’analyse d’Hessian et se présente sous la forme d’une cage thoracique sans ses os plats : le sternum, la clavicule et l’omoplate (voir **Figure 13**).

Cette segmentation pourrait être d’une grande aide dans notre projet afin d’aider les étudiants à mieux comprendre la position du cœur dans la cage thoracique. 


![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.025.jpeg)

**Figure 13 :** Graphique de l’algorithme de segmentation de la cage thoracique.** 


D’autres algorithmes permettent de diagnostiquer certains types de cancers. C’est notamment le cas d’une IA qui a été créée spécialement pour dépister les cancers du poumon à l’aide d’images scanner, de machine learning et de deep learning [29]. Cet algorithme est capable de différencier 3 types de cancers du sein (adénocarcinome, carcinome à large cellule, carcinome épidermoïde) et de donner le traitement adéquat. 

Dans le cadre de notre projet, nous n’aurions pas besoin d’un algorithme aussi fin capable de détecter la pathologie d’un organe, mais d’un algorithme apte à contrôler et à valider la position d’un organe complexe tel que le cœur. Cela a déjà été réalisé pour comprendre la morphologie, la localisation et la couvrance par la cage thoracique des organes abdominaux dans les positions assises et allongées dans le cadre d’un accident de voiture (voir **Figure 14**) [30]. 

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.026.jpeg)

**Figure 14 :** Vue en projection et méthode d’estimation du couvrage de la cage thoracique.

Une autre méthode pour pouvoir détecter la position et la localisation du cœur et de la cage thoracique dans une image médicale est la segmentation 3D.

La segmentation d'images 3D consiste à isoler les régions d'intérêts sur des images médicales telles que les IRM, CT scan… Ces régions sont certaines parties du corps humain et la segmentation permet de faciliter l'analyse de ces régions ou d'une caractéristique spécifique. [31]

La segmentation du cœur et de la cage thoracique (c'est-à -dire déterminer les contours du cœur dans des images médicales tels que les CT scan) va être plus ou moins précise et la méthode va différer en fonction du type et de la qualité de l'image, ainsi que du type de sujet et d'autres facteurs. La segmentation peut également varier en fonction de si nous voulons faire une segmentation plus ou moins manuelle. 

Afin de débuter une segmentation il faudrait faire un prétraitement de l'image, c'est-à-dire filtre l’image (avec par exemple des filtres de gauss) pour éliminer le bruit et les artefacts de numérisation, ou pour améliorer le contraste.

Il faut par la suite décider de la manière de segmentation (la segmentation par seuillage, la segmentation par contour actif, la segmentation par graphes, etc). Une fois la méthode définie, il faut utiliser des librairies de traitement d’image. 

La librairie la plus connue est OpenCV : c’est une librairie Open-Source qui peut être utilisée dans différents langages de programmations comme le python ou encore le C++. Nous nous intéresserons au C++ étant donné que c’est le langage qui est le plus utilisé sur CamiTK. OpenCV permet de faire de la segmentation d'images, de la détection de contours et de la reconnaissance de formes. [32]

Une autre librairie aussi utilisée par CamiTK est ITK (Insight Segmentation and Registration Toolkit) : C’est une librairie Open-Source pour le traitement d'images spécialisé dans le domaine médical et la recherche en imagerie. Cette librairie peut être utilisée en C++. Elle offre la possibilité de faire de la segmentation mais aussi de la reconstruction 3D... [33]

Il existe aussi VTK (Visualization Toolkit) : C’est une librairie open-source en C++ pour la visualisation et le traitement d'images spécialisé dans le domaine et la recherche médicale. Elle offre la possibilité de faire de la segmentation, de l'interaction avec l’utilisateur mais aussi de la reconstruction 3D…[34]

Enfin, il existe SimpleITK : c’est une librairie simplifiée d'ITK, qui permet aux débutant de prendre plus facilement en main la librairie. Cette librairie possède une interface simplifiée. Elle permet de faire du traitement d'images médicales, de la segmentation et du filtrage. [35]

Vous avez possibilité de trouver également des exemples de codes pour faire de la segmentation 3D sur GitHub. [36]

## <a name="_z0gbs37u81d6"></a>Visualisation imagerie

Durant notre projet nous voulons pouvoir visualiser des images médicales notamment des scanners. Ainsi, nous avons regardé ce qu’il existait déjà en termes de logiciel de visualisation. Il y a : 

- 3D Slicer : Un logiciel gratuit et Open Source qui permet la visualisation, le traitement et la segmentation d’images médicales telles que les CT-Scan. Ce logiciel est appliqué à la recherche, la pratique clinique et la reconstruction 3D des images médicales. Voir **Figure 15.** [37] 

**Figure 15 :** 3D Slicer![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.027.png)

- ITK-SNAP : Un logiciel gratuit et Open-Source TK-SNAP. Il a été développé à l'université de Caroline du Nord par des équipes d'étudiants dirigées par Guido Gerig (NYU Tanden School of Engineering). C’est un logiciel facile d’utilisation qui permet la segmentation d'images 3D et 4D. La segmentation d’image est semi-automatique à l'aide de méthodes de contour actif de délimitation manuelle et de navigation dans l'image. Voir **Figure 16.** [38]

**Figure 16 :** ITK - SNAP![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.028.png)![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.029.png)

- OsiriX : Un logiciel de visualisation d'imagerie médicale pour Mac. Il coûte $69.99/mois C’est le visualiseur DICOM le plus utilisé au monde. L’utilisation de ce logiciel est certifié pour l’usage médical. Il permet la visualisation des scanners et permet de faire de la segmentation. Il est utilisé dans les cliniques et les hôpitaux pour la planification des traitements et la recherche. Voir **Figure 17.** [39]

![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.030.png)

**Figure 17 :** OsiriX


- AMIRA : Un logiciel de visualisation, de segmentation et d'analyse d'images 3D qui permet de visualiser des scanners et de faire de la segmentation. Il est utilisé dans les domaines de la biologie, de la médecine et de l'ingénierie. Voir **Figure 18.** [40]

**Figure 18 :** AMIRA Software![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.031.png)


Nous avons décidé pour notre projet d'utiliser le logiciel CamiTK. En effet, c’est un logiciel que M Promayon nous a recommandé et il répond à toutes les exigences que nous avons. 

CamiTK (Computer Assisted Medical Interventions Toolkit) est un projet open-source écrit en majorité en C++ afin d’aider à la compréhension des images médicales, des navigations chirurgicales et des simulations biomédicales.

Ce toolkit a été développé à l’université de Grenoble dans le laboratoire TIMC-IMAG. Développées en interne depuis 2001, les premières lignes de codes ont été écrites en 2011 et il n’y a pas eu de modifications depuis 2 ans. Il peut être utilisé sur linux, windows et OS

Le manager de ce projet est Emmanuel Promayon et il a été aidé par 31 autres développeurs pour mener à bien ce projet. 

“CamiTK a 6 objectifs principaux : 

\- Prototyper des applications médicales 

\- Collecte de connaissances 

\- Intégration facile d'algorithmes, de données et de dispositifs 

\- Transfert technologique rapide 

\- Source libre et gratuite 

\- Développement multiplateforme” [41] 

Les utilisateurs ciblés sont les ingénieurs R&D, les étudiants ou les cliniciens.

CamiTK-imp permet de visualiser des images médicales à partir d'un grand nombre de formats, permet de modifier tous les jeux de données d’image et propose des algorithmes de traitement d'image et de segmentation grâce à des extensions de CamitK. 

Nous pouvons visualiser différents formats d’images (mha,DICOM), des maillages 3D(vtk, off,msh…) et des CTscan avec des coupes axial, coronal et sagittal ce qui nous permet d’obtenir une visualisation 3D.

Dans CamiTK la bibliothèque embarquée Insight Toolkit (ITK) permet de faire du filtrage et de la segmentation d’image. L’utilisateur peut régler chaque paramètre de l'algorithme.

De plus, le framework CamiTK permet aux développeurs d’ajouter des algorithmes existants ou encore des nouveaux algorithmes.Voir **Figure 19.** [42]![](Images/Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.032.png)

**Figure 19 :** Interface CamiTK

## <a name="_o65d05a1yd2w"></a>Visualisation 3D

Concernant l’analyse de l’existant de la visualisation logicielle en 3D,l’Université Paul-Sabatier de Toulouse a développé une application interactive en 3D pour l’apprentissage en 3D de l’ostéologie canine. [43] 

Pour la création d'images en 3D, ils ont importé les fichiers DICOM afin d’avoir accès aux données tomodensitométriques. La section choisie peut ensuite être visualisée en 3D pour ensuite  extraire les données nécessaires à la création de l’objet 3D de la structure souhaitée.Il est ensuite nécessaire d’exporter cet objet 3D dans un format numérique lisible par le logiciel de création graphique. Le format choisi est le .obj car un des plus répandus. Le choix du fichier dépendra des logiciels utilisés 

Pour la production graphique , l’outil 3ds Max® a été utilisé. 3ds Max® est  un outil de dessin 3D , son utilisation est indispensable afin de modéliser les objets 3D récupérés à partir de 3DSlicer.La reprise ainsi que l’exportation ,permettent donc de diminuer considérablement la résolution de l’objet, et donc de faciliter son affichage lors de l’utilisation de l’application finale

Il existe plusieurs procédures dans 3ds Max permettant de modéliser des objets 3D : soit à partir de formes dites primitives (carré, cylindre, sphère…), soit à partir de courbes. Ensuite , afin que chaque objet soit habillé d’une structure , ils appliquent une image créée sur ordinateur afin d’ajouter un effet secondaire de matière  à l’objet 3D brut.

Un outil de dessin simple numérique, tel que Photoshop®, a été utilisé  pour créer des texture des différents objets .Photoshop a également été utilisé pour créer les icônes du logiciel ainsi que les graphismes de l’interface.

Enfin , ils ont utilisé UNITY comme l’outil de développement .Ce dernier utilisant le langage C#.

Après avoir fait des recherches , aucune information n’a été trouvée sur l’intégration des capteurs dans ces logiciels préexistants. Nous avons donc regardé comment nous aurions pu faire correspondre l’interface avec les capteurs en le codant.



**1ère étape :** Récupérer les données du capteur (données spatiales)



- À l'aide d'une bibliothèque:  Sensorsapi.lib  en appelant[ ](https://learn.microsoft.com/fr-fr/windows/win32/api/sensorsapi/nf-sensorsapi-isensor-getdata)[ISensor::GetData](https://learn.microsoft.com/fr-fr/windows/win32/api/sensorsapi/nf-sensorsapi-isensor-getdata) Sensorsapi.lib



**2ème étape :** Construire l'image [44], [45], [46], [47]



- En utilisant la librairie Clmg: bibliothèque C++ libre



CImg est constituée de trois classes principales :

- CImg<T> : classe dédiée à la manipulation d’images de une à quatre dimensions, chaque scalaire étant de type générique T ;
- CImgList<T> : classe de manipulation de séquence d’images ;
- CImgDisplay : classe d’affichage d’images. Il est possible de capturer les interactions de l’utilisateur via la souris ou le clavier.



- Il y a également la possibilité d'utiliser ligibl 



**3ème étape**: Coder l'interface [48]

Après discussion avec le client, nous avons décidé que le meilleur moyen de mener à bien le projet était d’utiliser la visualisation des images 3D sur la plateforme camiTK dont nous avons déjà parlé auparavant. En effet, il est maintenant possible de visualiser chaque partie comme un objet 3D comme dans la **figure 1**. On ajoutera alors le code qui permet de récupérer les données du capteur au logiciel open source camitk.


###




<a name="_algrqulo83dr"></a>**Figure 20 :** Interface de CamiTK![](Aspose.Words.5961a69e-eaf2-412f-8d48-be5f46c1f0a9.033.png)
