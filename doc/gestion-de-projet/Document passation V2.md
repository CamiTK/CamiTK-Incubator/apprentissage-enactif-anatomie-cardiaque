---
title: "CamiTK White Paper on ..."
id: "compte-rendu-style"
puppeteer:
    format: "A4"
    puppeteer: ["pdf", "png"]
export_on_save:
    puppeteer: true
--- 

![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.003.png)![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.004.png)









<a name="_9171mm1qih4x"></a>**Document de passation**





# <a name="_62c51g6h6ppr"></a>Projet PFE : Structuration d’un TP d’apprentissage  énactif sur l’anatomie cardiaque 
















Ce document permet aux étudiants qui vont reprendre le projet d’apprentissage énactif avec ECCAMI de mieux comprendre le projet, les tâches faites et ce qu’il reste à faire. 

Nous avons essayé de détailler au mieux le contexte et nos tâches afin que la passation se passe au mieux et que vous n’ayez pas de difficulté à reprendre le projet. 

Ce document doit être complété avant chaque passation de projet.

A titre informatif, ce projet a débuté en février 2023 par un groupe de 4 TIS 5 et a continué par un autre groupe de 4 TIS 5 Cpro.



Historique du document



<table><tr><th valign="top"><b>Version</b></th><th colspan="2" valign="top"><b>Rédigé par</b></th><th colspan="2" valign="top"><b>Vérifié par</b> </th><th colspan="2" valign="top"><b>Validé par</b> </th></tr>
<tr><td rowspan="2" valign="top"><p> </p><p>`     `1.0</p></td><td valign="top"><p>LAURANT Mathilde,</p><p>MORIN Emilie, VAGNEURFrançois, </p><p>IRDEL Julie</p><p></p></td><td valign="top">10/03/23</td><td valign="top"><p>MORIN Emilie</p><p></p><p>IRDEL Julie</p></td><td valign="top">20/03/2023</td><td valign="top"></td><td valign="top"></td></tr>
<tr><td colspan="6" valign="top"><b>Motif et nature de la modification</b> : Création du document</td></tr>
<tr><td rowspan="2" valign="top"><p>    </p><p>`     `1.1</p></td><td valign="top">ELHABASHY Rodaina</td><td valign="top">13/06/2023</td><td valign="top"><p>LETOURNEAU Léna</p><p></p><p>KASSA Seuad</p></td><td valign="top">16/06/2023</td><td valign="top"></td><td valign="top"></td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>II. Solutions proposées.</p><p>A. Solution globale.</p><p>B. Différentes versions</p><p>II.Les technologies nécessaires</p><p>A. les capteurs</p><p>B. Visualisations 3D</p><p>IV. Bilan.</p><p></p></td></tr>
<tr><td rowspan="2" valign="top">`    `1.2</td><td valign="top">LETOURNEAU Léna</td><td valign="top">22/06/2023</td><td valign="top">Auteur</td><td valign="top">Date</td><td valign="top">Auteur</td><td valign="top">Date</td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>V - Annexes : Ajout de la partie US GitLab</p></td></tr>
<tr><td rowspan="2" valign="top">`    `2.0</td><td valign="top">RICEVUTO Léo</td><td valign="top">22/02/2024</td><td valign="top">TANG Elisabeth</td><td valign="top">Date vérif</td><td valign="top">MOUHAMADI Sity</td><td valign="top">Date validation</td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>V - Procédure d'utilisation</p></td></tr>
<tr><td rowspan="2" valign="top">`    `2.1</td><td valign="top">TANG Elisabeth</td><td valign="top">29/05/2024</td><td valign="top">RICEVUTO Léo</td><td valign="top">Date vérif</td><td valign="top">MOUHAMADI Sity</td><td valign="top">Date validation</td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>VI - Segmentation et traitement des données pour impression 3D</p></td></tr>
<tr><td rowspan="2" valign="top">`    `2.2</td><td valign="top">RICEVUTO Léo</td><td valign="top">30/05/2024</td><td valign="top">TANG Elisabeth</td><td valign="top">Date vérif</td><td valign="top">MOUHAMADI Sity</td><td valign="top">Date validation</td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>VII - Bugs connus</p></td></tr>
<tr><td rowspan="2" valign="top">`    `2.3</td><td valign="top">TANG Elisabeth, RAIS Oumaima</td><td valign="top">30/05/2024</td><td valign="top">RICEVUTO Léo</td><td valign="top">Date vérif</td><td valign="top">MOUHAMADI Sity</td><td valign="top">Date validation</td></tr>
<tr><td colspan="6" valign="top"><p><b>Motif et nature de la modification :</b> </p><p>VII - Conception et traitement des meshs pour impression 3D et IX - Suite du projet</p></td></tr>
</table>





**SOMMAIRE**


[**Projet PFE : Structuration d’un TP d’apprentissage  énactif sur l’anatomie cardiaque**](#_62c51g6h6ppr)

[**I - Présentation du projet**](#_cb7bhnjggmr1)

[A -  Contexte	](#_1vzbx9tgtv7u)

[B - Objectif	](#_dq8ll2qyn6wd)

[**II - Solutions proposées**](#_4f8g7r3cxe6z)

[**III - Les technologies nécessaires**](#_pjhp652bwaqq)

[A - Les capteurs	](#_c3fiflbaxmf8)

[B -  Visualisation 3D	](#_h6mgxh7adszw)

[C - CamiTK	](#_zef4cedx9pv1)

[**IV - Bilan**](#_10nfiqeoepam)

[**V - Procédure d'utilisation**](#_userproc)

[A - Récupération du code (via git)](#_codegit)

[B -  Démarrage CamiTK](#_camilaunch)

[C - Utilisation d'une extension](#_useextend)

[D - Architecture du projet	](#_archiproj)

[**VI - Segmentation et traitement des données pour impression 3D**](#_traitementseg)

[A - Installation de 3D slicer et des extensions](#_3dslicer)

[B -  Segmentation](#_seg)

[C -  Segmentation manuelle](#_segm)

[D - Lissage du modèle](#_lissage)

[E - Décimation du mesh	](#_decimation)

[**VII - Conception et traitement des meshs pour impression 3D**](#_traitementseg)

[A - Installation de 3DBuilder](#_3dbuilder)

[B -  Utilisation du logiciel](#_utilisationlog)

[C - Installation de Cura](#_cura)

[D - Paramétrage des impressions](#_parametreimpression)

[**VIII - Bugs connus**](#_troubleshootings)

[A - Plantage de CamiTK](#_plantage)

[B -  Problèmes liés au capteur](#_probcapteur)

[**IX - Suite du projet**](#_suite)

[A -  Pistes dans le cas où la cage thoracique n'est pas imprimée](#_pistes)

[**X - Annexes**](#_4fn8mla9l5lg)






# <a name="_cb7bhnjggmr1"></a>I - Présentation du projet
## <a name="_1vzbx9tgtv7u"></a>A -  Contexte

Ce projet a été attribué à des étudiants de la filière TIS de Polytech Grenoble. Les clients de ce projet sont l’école d'ingénieurs **Polytech Grenoble** et l’association **ECCAMI** : Excellence Center for Computer Assisted Medical Intervention. ”ECCAMI est un centre d’excellence hyperspécialisé regroupant cliniciens, chercheurs et industriels. Il est dédié à l’amélioration et à la valorisation des interventions médicales assistées par ordinateur.”[1]

“Polytech Grenoble est une école d’ingénieurs de Grenoble INP, Institut d’ingénierie et de management de l'Université Grenoble Alpes. Membre du réseau Polytech, c’est une école publique habilitée par la Commission des Titres d’Ingénieur.”[\[2\]](#_4fn8mla9l5lg)

Ce projet a pour objectif de développer un système d**'apprentissage énactif** pour l'enseignement de l'anatomie humaine pour les étudiants de TIS. L'apprentissage énactif est une forme d'apprentissage qui se base sur **les activités multi sensorielles motrices pour favoriser un apprentissage plus rapide et plus profond**. Il permet aux apprenants d'acquérir des connaissances de manière active et immersive en utilisant leurs capacités motrices et sensorielles pour comprendre les concepts.

En d’autres termes, nous voulons créer un TP pour les étudiants de TIS durant lequel ils manipulent un cœur imprimé en 3D afin de mieux comprendre et mieux appréhender la localisation et l’orientation du cœur dans la cage thoracique. Ce projet vise plus précisément à comprendre et à trouver **la localisation et l'orientation du cœur dans la cage thoracique de manière ludique.** L'objectif est d'offrir aux étudiants une meilleure vision et compréhension du cœur chez le patient sain qui leur permettra d'acquérir des connaissances anatomiques et physiologiques de manière plus active. Pour cela nous voulons que cet apprentissage se fasse sous la forme d’un jeu durant lequel les étudiants pourront manipuler le cœur en 3D. Ce projet permettra ainsi de surmonter les limites de l'enseignement traditionnel de l'anatomie basé sur des manuels ou des images 2D, en offrant **une alternative plus ludique aux étudiants.** Enfin, de manière plus globale, ce projet permettra de sensibiliser la population à cet organe essentiel pour notre santé et le bon fonctionnement de notre corps















## <a name="_dq8ll2qyn6wd"></a>B - Objectif

- Développer un système d'apprentissage énactif pour les étudiants TIS afin d’apprendre l'anatomie humaine.

- Proposer aux étudiants une meilleure compréhension de la localisation et de l'orientation du cœur dans la cage thoracique grâce à un cœur imprimé en 3D et à un squelette de cage thoracique.

- Placer le cœur dans la cage thoracique avec un retour immédiat sur l'ordinateur afin de s’assurer de son bon emplacement.

- Éduquer plus généralement la population (autres que les chirurgiens) sur l'emplacement du cœur dans la cage thoracique afin de les sensibiliser à cet organe central.

- Réaliser ce projet en partenariat avec l'association ECCAMI afin de bénéficier de l'expertise de cliniciens, chercheurs et industriels dans le domaine de l'amélioration et de la valorisation des interventions médicales.

## <a name="_k28wa1vzkt4o"></a>
# <a name="_4f8g7r3cxe6z"></a>II - Solutions proposées
## <a name="_cro3cnt74hik"></a>A - Solution globales

L’objectif du TP est de permettre aux apprenants une meilleure représentation de l’anatomie cardiaque. Sous une forme de mini jeu, les participants vont tour à tour regarder les images médicales sous plusieurs angles afin de positionner correctement le cœur. Ces images seront affichées sur un logiciel propre au TP,CamiTK, qui servira ensuite pour la validation de la position des objets manipulables. Ils prendront ensuite les objets en main et les placeront dans la cage thoracique selon leurs estimations. Quand ils seront satisfaits du placement du cœur, ils valideront sa position sur le logiciel. Un score sera ensuite déterminé en fonction de la distance effective entre la position de l’objet et celle du cœur du sujet ou en fonction du temps mis pour atteindre la cible.


Une étape de calibration est nécessaire afin que la position de l’objet manipulable soit la plus précise possible. 

![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.005.png)

*Figure 1: Vue d’ensemble du projet.*

B - Différentes versions

Nous proposons de faire 5 versions différentes pour notre projet. Le but de faire 5 versions est de pouvoir réaliser le projet en le découpant en sous tâche. Chaque version est autonome mais les 5 versions sont toutes liées entre elles. 

La Version 0 (V0) sera la plus simple à réaliser, elle ne nécessitera que l’utilisation d’une interface (absence de capteur) dans le logiciel CamiTK. Ainsi, la cage thoracique et le cœur seront modélisés en 3D grâce aux outils déjà implémentés dans CamiTK. Plusieurs niveaux seront proposés à l’utilisateur afin d’augmenter la difficulté. Ces niveaux seront présents dans toutes les versions et un score sera calculé à la fin de chaque partie. Ce score prendra en compte le temps écoulé, la “finesse” des mouvements de l’utilisateur et l’exactitude des résultats. Pour garder un niveau de difficulté assez élevé, nous avons décidé qu’il y aurait les 6 degrés de liberté dès la V0 (voir Design du jeu pour en apprendre davantage sur cette partie dans le cahier des charges). Pour moyenner les résultats du jeu, nous avions pensé à introduire 3 manches (3 rounds) au jeu, après quoi, le score de l’utilisateur sera calculé et s’affichera. 

**Résumé V0 :** 

- Absence de capteur
- Cage thoracique et coeur 3D visibles dans CamiTK
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 

La V1 permettra par la suite d'introduire un capteur à la première version. Les utilisateurs pourront ainsi bouger directement le “cœur”, représenté par un seul capteur, dans l’espace. Pour cette version, le coeur ne sera pas imprimé en 3D, on va utiliser un ballon pour représenter ce dernier.Cette étape introduira de nouvelles difficultés quant à l’exactitude de la retranscription du mouvement et de la position du cœur dans le logiciel. La V2, quant à elle, permettra d’ajouter des images médicales au logiciel (voir Veille pour comprendre le choix d’imagerie dans le cahier des charges). Cela donnera l’occasion aux utilisateurs d’avoir accès à d’autres informations (notamment sur les tissus environnants) et d’apprendre à visualiser des images médicales. L’introduction de la fonctionnalité “Changement d’orientation de la cage thoracique sur le logiciel entre chaque essai”, sera étudiée par la suite selon la difficulté de son implémentation. 

**Résumé V1 :** 

- Un capteur
- Cage thoracique et coeur 3D visibles dans CamiTK
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 

**Résumé V2 :** 

- Un capteur
- Cage thoracique et coeur 3D + images médicales visibles dans CamiTK 
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 

Dans la V3, nous augmenterons la difficulté en introduisant un cœur imprimé en 3D. Cela nécessitera davantage de capteurs. Après avoir recherché dans la littérature scientifique, nous avons décidé qu’il faudra utiliser du PLA (acide polylactique) et un dépôt de fil fondu ou une extrusion (FDM, FFF) (voir “**Autres aspects du projet**” - “Veille” - “Impression 3D” dans le cahier des charges). Pour rendre le jeu plus réaliste, l’ajout d’une base de données a été étudié afin de stocker les meilleurs scores ainsi que les pseudos des étudiants. Ce dernier point reste optionnel, le plus important reste la bonne intégration des capteurs dans la version. 

**Résumé V3 :** 

- Plusieurs capteurs sur un coeur imprimé en 3D 
- Cage thoracique et coeur 3D + images médicales visibles dans CamiTK 
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 
- Base de données 

La dernière version sera la plus complexe à réaliser. À la V3 nous ajouterons une cage thoracique physique, ce qui permettra une meilleure visualisation et un apprentissage plus concret. Il sera également envisageable d'intégrer un système de feedback par le cœur imprimé par l’ajout de lumières ou des vibrations. Cette version a été le moins travaillée, il sera donc important d’approfondir les recherches déjà effectuées.  

**Résumé V4 :** 

- Plusieurs capteurs sur un coeur imprimé en 3D 
- Cage thoracique et coeur 3D + images médicales visibles dans CamiTK 
- Cage thoracique physique pour placer le coeur 
- Plusieurs niveaux de jeu
- 3 manches
- 6 degrés de liberté 
- Base de données 

Le tableau ci-dessous résume les différentes fonctionnalités de chacune de nos versions. 

|*Versions*|**V0**|**V1**|**V2**|**V3**|**V4**|
| -: | :-: | :-: | :-: | :-: | :-: |
|*Fonctionnalités*||||||
|Positionnement 3D du coeur dans le logiciel **CamiTK**|✅|✅|✅|✅|✅|
|Positionnement 3D du coeur dans le logiciel avec un **capteur** ||✅|✅|✅|✅|
|Positionnement 3D du coeur dans le logiciel avec un coeur imprimé en 3D (**plusieurs capteurs**)||||✅|✅|
|||||||
|Visualisation de la cage thoracique 3D et du coeur dans **CamiTK**|✅|✅|✅|✅|✅|
|Ajout d’**images médicales** |||✅|✅|✅|
|Utilisation d’une cage thoracique **physique**|||||✅|
|||||||
|**Feedback** dans l’écran de l’interface|✅|✅|✅|✅|✅|
|**Feedback** par le coeur 3D (vibration, lumière)|||||✅|
|||||||
|**6** degrés de liberté du cœur|✅|✅|✅|✅|✅|
|||||||
|Mise en place de **3 manches** pour le calcul du score d’une personne|✅|✅|✅|✅|✅|
|Possibilité de **bouger** la cage thoracique sur le logiciel|✅|✅|✅|✅|✅|
|Changement d’orientation de la cage thoracique sur le logiciel entre chaque essai (à voir à l’implémentation)|||✅|✅|✅|
|Ajout de pseudo et stock des meilleurs scores (optionnel)||||✅|✅|

**Tableau 1 :** Représentation des différentes versions du projet

# <a name="_pjhp652bwaqq"></a>III - Les technologies nécessaires
## <a name="_c3fiflbaxmf8"></a>A - Les capteurs

Une des options permettant la localisation 3D d’un objet est l’utilisation de capteurs électromagnétiques. Ceux-ci mesurent la force du champ magnétique au point de l’espace où ils se situent [3]. 

`	`Ils sont constitués de plusieurs parties :

- un émetteur : composant qui produit produit 3 champs magnétiques orthogonaux
- un ou plusieurs récepteurs : chacun est constitué de 3 bobines orthogonales, dont la tension est alors due aux champs magnétiques de l’émetteur

Généralement, l’émetteur a une position fixe et les récepteurs sont fixés sur la cible dont on veut la localisation 3D.

M.Promayon nous a recommandé l’utilisation du capteur électromagnétique Flock of Birds (FoB) par Ascension Technology Corporation. C’est un capteur EM de tracking très utilisé en réalité virtuelle, simulation et robotique. Il a l’avantage de correspondre à nos attentes en termes de précision et de résolution.

Leur production a été stoppée, leur produit de remplacement est le 3D Guidance trakSTAR / driveBAY.

Après plusieurs essais, nous n 'avons pas pu faire fonctionner le capteur. Il faudra donc en acheter un autre. 

## <a name="_h6mgxh7adszw"></a>B -  Visualisation 3D

Afin de pouvoir visualiser le cœur imprimé en 3D nous devons récupérer les données recueillies par le capteur. les étapes sont :  

1) Récupérer les données du capteur (données spatiales)
- La bibliothèque et le logiciel à utiliser seront déterminés en fonction du capteur.



1) Construire l'image à l’aide des données du capteur
- En utilisant la librairie Clmg: bibliothèque C++ libre. CImg est constituée de trois classes principales :
- **CImg<T>** : classe dédiée à la manipulation d’images de une à quatre dimensions, chaque scalaire étant de type générique T ;
- **CImgList<T>** : classe de manipulation de séquence d’images ;
- **CImgDisplay** : classe d’affichage d’images. Il est possible de capturer les interactions de l’utilisateur via la souris ou le clavier. [4,5,6]

Une fois que les données  sont recueillies, il faut tout intégrer à CamiTK.






## <a name="_zef4cedx9pv1"></a>C - CamiTK

CamiTK (Computer Assisted Medical Interventions Toolkit) est un projet open-source écrit en majorité en C++ afin d’aider à la compréhension des images médicales, des navigations chirurgicales et des simulations biomédicales. [\[8\]](#_4fn8mla9l5lg)

CamiTK-imp permet de visualiser des images médicales à partir d'un grand nombre de formats, permet de modifier tous les jeux de données d’image et propose des algorithmes de traitement d'image et de segmentation grâce à des extensions de CamitK. 

Nous pouvons visualiser différents formats d’images (mha,DICOM), des maillages 3D(vtk, off,msh…) et des CTscan avec des coupes axial, coronal et sagittal ce qui nous permet d’obtenir une visualisation 3D.

Dans CamiTK la bibliothèque embarquée Insight Toolkit (ITK) permet de faire du filtrage et de la segmentation d’image. L’utilisateur peut régler chaque paramètre de l'algorithme.

Un document d’installation de CamiTK  a été rédigé.
# <a name="_10nfiqeoepam"></a>IV - Bilan
Durant ce PFE, nous avons préparé plusieurs documents qui vous seront utiles pour le démarrage de votre PFE. Notre objectif était de comprendre pleinement le sujet et de rédiger tous les documents nécessaires afin que vous puissiez entamer la partie technique de ce projet sans avoir à consacrer du temps à des recherches supplémentaires. De plus, ces documents ont été conçus pour vous éviter des problèmes d'installation potentiels.

Les documents qui ont étés préparés en plus du document de passation sont les suivants:

- **Un cahier des charges** afin que vous n’ayez pas à chercher les spécifications. Dans le cahier des charges, vous retrouverez des points similaires avec ce document de passation mais vous retrouverez également une analyse de l’existant, les détails des versions , des informations sur le capteur, les logiciels à utiliser et les détails du design du jeu. 
- **Document d’installation CamiTK**, afin d’éviter les problèmes d’installations.Dans ce document vous trouverez en détails les étapes d’installations avec des vidéos dont une réalisée par notre professeur PROMAYON Emmanuel.
- **Document Test du capteur Flock of Birds**. Ce document résume les tests et les problèmes qu’on a rencontrés avec le capteur Flock of Birds qui nous a été fourni.

On a également rédigé **les User Story** qui sont déposées** sur GIT **[\[9\]](#_4fn8mla9l5lg)** (Voir [A - Issues sur GitLab](#_lhhyii8ylk3y)) les User Story présentent une explication non formelle, générale des fonctionnalités.Vous trouverez une liste de toutes les tâches à réaliser en détails.On a aussi a préciser les user story qui ont déjà été réalisées et ceux qui vous restent à faire durant votre Projet. 

Tous les document ont été mis sur git en langage md (markdown) et déposés sur Git.

Nous vous recommandons de suivre ces étapes pour faciliter votre travail lors de la réalisation de la partie technique :

- Commencez par une lecture approfondie du cahier des charges afin de bien comprendre le projet et le travail à effectuer.
- Ensuite, prenez le temps de lire et de bien comprendre les User Stories. Cela vous permettra d'avoir une vision claire des fonctionnalités attendues.
- Pour gagner du temps, suivez le guide d'installation CamiTK qui a été fourni. Il vous fournira des instructions détaillées pour configurer l'environnement de développement.
- Enfin, lisez attentivement le document relatif au capteur Flock of Birds. Cela vous permettra de comprendre son fonctionnement..

En suivant ces étapes, vous serez mieux préparé et plus efficace dans la réalisation de la partie technique.

**    Nous souhaitons que ce document vous soit réellement utile pour simplifier la réalisation de la partie technique.


# <a name="_userproc"></a>V - Procédure d'utilisation
Ce chapitre a pour objectif de vous lancer pas à pas dans ce projet. On part du principe que vous avez déjà une machine prête à l'emploi avec CamiTK et Visual Studio Code installé

Le mot de passe pour accéder aux ordinateurs Linux est : tis@polytech


## <a name="_codegit"></a>A - Récupération du code (via git)
Dans un premier temps vous allez récupérer le code sur git. Rendez vous sur git dans ce répertoire :
![](Images/Screenshot_20240222_104048.png)
Maintenant, cliquez sur "Code" et copiez le lien "Clone with HTTPS"
![](Images/gitscreen1.png)
Ouvrez une Konsole, positionnez vous dans le répertoire où vous souhaitez cloner le projet.
Utiliser la commande "git clone" suivi du lien que vous avez copier. 
![](Images/Screenshot_20240222_105232.png)
Vous disposez désormais du projet en local sur votre machine.

## <a name="_camilaunch"></a>B - Démarrage CamiTK
Afin de faciliter le lancement de CamiTK, un fichier Json a été créer. Ce fichier vous permets de démarrer camiTK directement depuis VSCode.
![](Images/Screenshot_20240222_113026.png)
Pour démarrer CamiTK, cliquez sur l'icone "Run and bug", puis cliquez sur le bouton vert ou sur le raccourci F5
![](Images/rundebug.png)

## <a name="_useextend"></a>C - Utilisation de l'extension et du capteur AmfiTrack
Pour pouvoir utiliser une extension, vous devez commencer par ouvrir un fichier .camitk, dans cet exemple nous utilisons le fichier "amfitrack-on-calibrator.camitk". Il faut également mettre en route le capteur AmfiTrack, pour ce faire branchez le hub "H3" en USB sur votre PC, branchez à l'aide de la prise secteur l'émetteur "TX3" et allumer un des 3 capteurs "RX3" disponibles.
Une fois le fichier ouvert et le capteur mis en route, clique droit sur le fichier "AmfiTrack Magnetic Sensor", dans la section "Anatomy" sélectionner notre extension "Placement Coeur".
![](Images/camiextension.png)
Désormais l'extension est chargée dans Camitk et prête à l'emploi, pour commencer à l'utiliser il vous suffit d'appuyer sur le bouton "Apply"

## <a name="_archiproj"></a>D - Architecture du projet
Si vous souhaitez effectuez des modifications dans le code de l'extension, vous pourrez retrouvez les fichiers "PlacementCoeur.cpp" et "PlacementCoeur.h" dans le projet "apprentissageenactif" dans le dossier "actions" puis dans sous dossier "apprentissageenactif".
![](Images/archicode.png)


# <a name="_traitementseg"></a>VI - Segmentation et traitement des données pour impression 3D
Pour le bon déroulement de ce projet, il est nécessaire de segmenter les images scanner et de récupérer la cage thoracique et le coeur issu du jeu de données "amos_0218-flipped.mha" fourni par le client.
La cage thoracique est composée du :
* sternum
* côtes gauches
* côtes droites
* vertèbres thoraciques
* cartilages costaux


## <a name="_3dslicer"></a>A - Installation de 3D slicer et des extensions
La segmentation nécessite l'installation de 2 éléments :
* [3D slicer](https://www.slicer.org/)
* Extension : TotalSegmentator

Suivez la partie "Extensions Manager" de ce [tutoriel](https://www.slicer.org/wiki/Documentation/4.3/SlicerApplication/ExtensionsManager) pour installer l'extension "TotalSegmentator" qui se chargera de segmenter automatiquement les données entrées.

Après les segmentations, le coeur et la cage thoracique doivent être lisses et décimés pour faciliter l'impression 3D. La décimation réduit le nombre d'arêtes, sommets et triangles qui composent le maillage.

Le lissage et la décimation du modèle nécessite l'installation d'un logiciel :
* [MeshLab](https://www.meshlab.net/)

## <a name="_seg"></a>B - Segmentation automatique
Après avoir téléchargé 3D slicer et l'extension :
1. Chargez le jeu de données
2. Segmentez : activez l'extension installée et appliquez le traitement

![](Images/seg1.PNG)

3. Dans data, exportez les éléments visibles dans model

![](Images/seg2.1.png)

4. Séléctionnez 2 éléments à fusionner

![](Images/seg2.png)

![](Images/seg3.png)

5. Séléctionnez l'élément précédent et répétez l'opération avec un autre élément
6. Exportez les données sous format model

 ![](Images/seg4.png)
 
 7. Exportez les données sous format stl
 
![](Images/seg5.png)

## <a name="_segM"></a>C - Segmentation manuelle
[Lien](https://www.youtube.com/watch?v=9KDynY6c9-w) du tutoriel youtube pour effectuer la segmentation de la cage thoracique
1. Chargez les données
2. Dans l'onglet Segment Editor, ajouteZ un segment 

![](Images/Capture1.PNG)


3. Séléctionnez l'effet "Threshold", faites varier les paramètres, cliquez sur apply lorsque résultat convient et cliquez sur "show 3D" pour avoir la représentation

![](Images/Capture2.PNG)

4. Supprimez les éléments qui ne vous intéressent pas avec l'effet "scissor"

## <a name="_lissage"></a>D - Lissage du modèle
1. Chargez les données
2. Lissez le modèle et choisissez le paramètre convenable

![](Images/seg6.png)


2. Lissage local

Lissez localement les défauts restants.

![](Images/seg9.png)

## <a name="_decimation"></a>E - Décimation du mesh
1. Visualisez le maillage
![](Images/seg7.png)


2. Décimez le maillage en choisissant le paramètre convenable (ordre de 20 000 target number face)

![](Images/seg8.png)

# <a name="_traitementseg"></a>VII - Conception et traitement des meshs pour impression 3D

## <a name="_3dbuilder"></a>A - Installation de 3DBuilder 
Dans le cadre de ce projet, il est nécessaire de manipuler et traiter des objets 3D. Que ce soit en terme de conception (exemple : création de support) ou de traitement (ajout de loge pour le sensor) le logiciel qui nous a paru le plus intuitif à utiliser est 3D builder. Il est accessible depuis le microsoft store et peut etre téléchargé sur les postes des salles d’informatique. Attention : afin de ne pas avoir à le télécharger à chaque redémarrage des postes, il est possible de l’installer sur C:/Temp. Il sera alors accessible à toutes les sessions des membres du groupe . 
![](Images/O1.png)

## <a name="_utilisationlog"></a>B - Utilisation du logiciel 
Des tutoriels sont disponibles en ligne pour appréhender le logiciel. 

Tutoriel rapide qui résume les principales fonctionnalités: 
https://www.youtube.com/watch?v=f5XLbznDkNA

Tutoriel pour rajouter le logo TIS sur le support: https://www.youtube.com/watch?v=cAxM0BPCtIU 

Pour résumer les fonctionnalités principales que nous avons utilisé lors de la conception, nous avons: 

![](Images/O2.png)

Soustraire : permet de creuser les objets avec les formes voulues. 
Grouper : permet de grouper les formes le temps de faire le traitement. 
Fusionner : permet de créer un objet 3D avec les différents objets sélectionnés. 

![](Images/O3.png)

Pinceau > Matériaux : permet d’afficher les objets en transparence. Permet de voir à l'intérieur des objets lors du traitement. 

![](Images/O4.png)

Pour importer les mesh dans 3Dbuilder Il faut faire attention à ce qu’ils soient bien en STL. Lors de l’import vous risquez d’avoir ce message d’erreur. Il suffit de cliquer dessus pour réparer les fichiers.  

![](Images/O5.png)

## <a name="_cura"></a>C - Installation de Cura
Une fois que vous avez vos modèles finaux en STL, il vous est possible de les imprimer à polytech (Labo polybot du premier étage). Avant de faire cela il vous faut paramétrer l’impression. Le logiciel Cura vous permet de personnaliser les paramètres d’impression et de convertir le scénario final au format gcode attendu par l’imprimante. 

![](Images/O6.png)

## <a name="_parametreimpression"></a>D - Paramétrage des impressions 
Lors de l’utilisation de cura, il faut préciser le type d’imprimante (à  polytech : Ultimaker3 Extended). Pour le paramétrage vous pouvez vous réferer au tutoriel suivant : https://www.youtube.com/watch?v=qHJSz4V7DJk 
Nous vous conseillons de vous faire accompagner par un des membres du polybot pour optimiser les paramètres.


# <a name="_troubleshootings"></a>VIII - Bugs connus
Dans ce chapitre nous listons les problèmes connus, leur cause, ainsi que leur solution. 

## <a name="_plantage"></a>A - Plantage CamiTK
Il arrive que CamiTK cesse de fonctionner, ou alors soit impossible à fermer. Ce problème est causé par la librairie Python qui gère le capteur, si vous obtenez le message d'erreur ci dessous, commencer par vérifier si vous avez bien brancher le capteur amfitrack (hub, capteur et émetteur).

![](Images/bug1.png)

Un autre bug pourrait survenir vous empêchant de fermer CamiTK et de l'utilisé, dans ce cas il vous suffit d'ouvrir une invite de commande, vous placez dans le répertoire apprentissage enactif ou l'extension du code se trouve et utiliser la commande "killall QThread"

![](Images/killthreads.png)

## <a name="_probcapteur"></a>B - Problèmes liés au capteur
Si le capteur est trop proche de l'émetteur, il va devenir rouge et ne plus être détecté par l'extension.
Si le capteur ne situe pas au bon endroit dans CamiTK, il faut le recalibrer en le placant (a peut prêt) à l'origine affichée par camitk et ensuite appuyé sur le bouton du capteur.

# <a name="_suite"></a>IX - Suite du projet
Pistes d'amélioration :
- Traitement du fichier stl de la cage thoracique pour combler les trous (refaire la segmentation)
- Impression 3D de la cage thoracique
- Peindre le coeur en rouge


## <a name="_pistes"></a>A - Pistes dans le cas où la cage thoracique n'est pas imprimée

- Redimensionner (en largeur) la cage thoracique segmentée 
- Améliorer l'interface
- Réflechir à un moyen pour repérer la position de la cage thoracique dans le logiciel pour que les mouvement de la cage thoracique ne pertube pas le jeu ( piste : ajouter un capteur à un endroit précis ) (ou fixer la cage thoracique)
- Optimiser le setup si la cage thoracique n'a pas été imprimée
- Imprimer un bouchon pour le coeur pour éviter que le capteur ne tombe pas



# <a name="_4fn8mla9l5lg"></a>X - Annexes

[1]http://eccami.imag.fr

[2] <https://www.polytech-grenoble.fr/menu-principal/l-ecole/>

[3] Bernhard Preim, Charl Botha, in Visual Computing for Medicine (Second Edition), 2014 : 18.4.4 Electro-Magnetic Tracking

[4][**https://learn.microsoft.com/fr-fr/windows/win32/api/sensorsapi/nf-sensorsapi-isensor-getdata**](https://learn.microsoft.com/fr-fr/windows/win32/api/sensorsapi/nf-sensorsapi-isensor-getdata)

**[5] [https://learn.microsoft.com/fr-fr/windows/win32/sensorsapi/retrieving-sensor-data-fields**](https://learn.microsoft.com/fr-fr/windows/win32/sensorsapi/retrieving-sensor-data-fields)**

**[6] [http://sparks.i3s.unice.fr/_media/public:gaignard:imagesio-2014.pdf**](http://sparks.i3s.unice.fr/_media/public:gaignard:imagesio-2014.pdf)**

**[7] [Images in c++ with CImg library: Draw, display and event loop.**](https://www.youtube.com/watch?v=Q9gqK4QEz3o)**

**[8] <https://www.openhub.net/p/camitk>**

**[9]<https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK-Incubator/apprentissage-enactif-anatomie-cardiaque/-/issues>** 




























## <a name="_lhhyii8ylk3y"></a>A - Issues sur GitLab
Tout d’abord voici le lien du [**GitLab**](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK-Incubator/apprentissage-enactif-anatomie-cardiaque). Si vous n’y avez pas accès, il faut demander à Mr. Emmanuel Promayon 

![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.006.png)


Lorsque vous arrivez sur le GitLab, vous pouvez retrouver toutes les User Story dans la partie “Issues”. Dans List, elles s’afficheront toutes dans l’ordre de leur dernière modification. Dans Board, elles seront répartis suivant des catégories. 













*Figure 2 : Barre latéral GitLab*

![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.007.png)*Figure 3 : Board Gitlab*

` `Si vous cliquez sur le menu déroulant où il est écrit “Développement” vous pourrez choisir de trier les issues par version. 

Dans le menu développement, elles sont rangées par User Story. 

![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.008.png)

*Figure 4 : Issue*

Une issue se présente comme ceci : 

- ` `Dans Overview, il y a une rapide présentation de l’issue à réaliser. 
- Dans Advancement, on peut noter l’avancement de cette issue. Cela permet aux groupes reprenant le sujet de savoir ce qui a déjà été fait.
- Sur le côté droit on retrouve les labels. Il est important d’y attacher celui avec le nom de l’US, le numéro de la version visée par cette issue et la taille de l’US. De plus, certaines issues nécessitent une connaissance de CamiTK, elles portent le tag “CamiTk”.

Certaines issues doivent être faites après avoir fini une autre User Story, ce sera indiqué dans Overview. 

Au sein d’une même US les tâches sont numérotées afin de les effectuer dans l’ordre. 

Evidemment, les US pour la V0 doivent être effectués avant ceux pour la V1, avant ceux pour la V2, etc. 

![](Images/Aspose.Words.4cde3830-0a02-4f17-ad5b-38369841bc26.009.png)

*Figure 5 : Exemple d’une Issue nécessitant d’en avoir fini une autre et utilisant CamiTK*

